import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

public class ReflectionMain {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Class cv = null;
        try {
            cv = Class.forName(scanner.next());
            Class cv2 = Class.forName(scanner.next());
            String methodName = scanner.next();
            Method m = cv.getMethod(methodName, cv2);
            Object o1 = cv.newInstance();
            Object o2 = cv2.newInstance();
            System.out.println(m.invoke(o1, o2));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}
