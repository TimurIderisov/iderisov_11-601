//task1g
public class Task1 {
    public static void main(String [] args) {
	    final double EPS = 1e-9;
		double p = 100;
		int i = 0;
		double x = Double.parseDouble(args[0]);
		double h = 0;
		double s = 0;
		double f = 33;
		double fac, h2;
		while (Math.abs(f - p) >= EPS) {
			p = f;
			h = 1 + x + x * x;
			fac = 1;
			for(int z = 1; z <= (2 * i + 1); z++) {
				fac = fac * z ;
			}
			h2 = h;
			for(int v = 1; v <= (2 * i + 1); v++) {
				h2 = h2 * h2;
			}
			f = (3 * h2) / fac; 
			s = s + f;
			i++;	
		}
		System.out.println(f);
	}
}	