package snake;
public class Food {
	SnakeCell sc = new SnakeCell();
	
	public SnakeCell generator() {
		sc.setX((int) Math.round(Math.random() * 10));
		sc.setY((int) Math.round(Math.random() * 10));
		return sc;
	}
	
}