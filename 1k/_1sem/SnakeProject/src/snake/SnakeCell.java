package snake;
public class SnakeCell {
	private int x;
	private int y;
	
	public SnakeCell() {
	}
	
	public SnakeCell(int a, int b) {
		x = a;
		y = b;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setX(int a) {
		x = a;
	}
	
	public void setY(int a) {
		y = a;
	}
	
}