package snake;
public class Field {
	char [][] field = new char[11][11];
	Snake sn = new Snake();
	Food f = new Food();
	int h = (f.generator()).getX();
	int l = (f.generator()).getY();
	
	
	public Field(){
		for(int  i = 0; i <= 10; i++) {
			for(int  j = 0; j <= 10; j++){
				field[i][j] = '-';
			}		
		}
	}
	
	public void render() {
		for(int i = 0; i <= 10; i++) {
			for(int  j = 0; j <= 10; j++) {
				field[i][j] = '-';
			}
		}
		for(int i = 0; i < sn.getLength(); i++) {
			int a = (sn.snakecells[i]).getX();
			int b = (sn.snakecells[i]).getY();
			field[b][a] = 'O';
		}
		field[l][h] = '@';
	}
	
	public void draw() {
		for(int  i = 0; i <= 10; i++) {
			for(int  j = 0; j <= 10; j++){
				System.out.print(field[i][j]);
			}		
			System.out.println();
		}
	}
	
	public void move(char dir) {
		if(dir == 'w') {
			if(sn.snakecells[0].getX() == h && (sn.snakecells[0].getY() - 1) == l) {
				helper2();
				sn.snakecells[0].setY(l);
				h = f.generator().getX();
				l = f.generator().getY();
			}
			else {
				helper();
				sn.snakecells[0].setY(sn.snakecells[0].getY() - 1);
			}
		}
		
		if(dir == 's') {
			if(sn.snakecells[0].getX() == h && (sn.snakecells[0].getY() + 1) == l) {
				helper2();
				sn.snakecells[0].setY(l);
				h = f.generator().getX();
				l = f.generator().getY();
			}
			else {
				helper();
				sn.snakecells[0].setY(sn.snakecells[0].getY() + 1);
			}
		}
		
		if(dir == 'a') {
			if((sn.snakecells[0].getX() - 1) == h && sn.snakecells[0].getY() == l) {
				helper2();
				sn.snakecells[0].setX(h);
				h = f.generator().getX();
				l = f.generator().getY();
			}
			else {
				helper();
				sn.snakecells[0].setX(sn.snakecells[0].getX() - 1);
			}
		}
		
		if(dir == 'd') {
			if((sn.snakecells[0].getX() + 1) == h && sn.snakecells[0].getY() == l) {
				helper2();
				sn.snakecells[0].setX(h);
				h = f.generator().getX();
				l = f.generator().getY();
			}
			else {
				helper();
				sn.snakecells[0].setX(sn.snakecells[0].getX() + 1);
			}
		}
	}
	
	public void helper() {
		for(int i = (sn.getLength() - 1); i > 0; i--) {
					sn.snakecells[i].setX(sn.snakecells[i - 1].getX());
					sn.snakecells[i].setY(sn.snakecells[i - 1].getY());
				}
	}
	
	public void helper2() {
		SnakeCell hg = new SnakeCell();
		sn.setLength();
		sn.snakecells[sn.getLength() - 1] = hg;
		helper();
	}
	
}