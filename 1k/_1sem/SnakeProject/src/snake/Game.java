package snake;
import java.util.Scanner;
public class Game {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Field fi = new Field();
		Food free = new Food();
		fi.render();
		fi.draw();
		char di;
		boolean on = true;		
		

		while(on) {
			di = reader.next().charAt(0);
			if(fi.sn.checkWell(di) || fi.sn.checkSelf()) {
				break;
			}
			else {
				System.out.println("###SNAKE##");
				fi.move(di);
				fi.render();
				fi.draw();
				System.out.println("#SCORE: " + fi.sn.getLength()+"##");
			}
		}
	}
}