package snake;
public class Snake {
	SnakeCell [] snakecells = new SnakeCell[121];
	int length = 2;
	
	public Snake() {
		SnakeCell sc = new SnakeCell(1,1);
		SnakeCell sc1 = new SnakeCell(1,0);
		snakecells[0] = sc;
		snakecells[1] = sc1;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength() {
		length += 1;
	}	
	
	public boolean checkSelf() {
		boolean check = false;
		for(int i = 1; i < length; i++) {
			if(snakecells[0].getY() == snakecells[i].getY() && snakecells[0].getX() == snakecells[i].getX()) {
				System.out.println("GAME OVER");
				check = true;
			}
		}
		return check;
	}
	
	public boolean checkWell(char dir) {
		boolean check = false;
		if((snakecells[0].getY() == 0 && dir == 'w') || (snakecells[0].getY() == 10 && dir == 's') || (snakecells[0].getX() == 0 && dir == 'a') || (snakecells[0].getX() == 10 && dir == 'd')){
			System.out.println("GAME OVER");
			check = true;
		} 
		return check;
	}
	
}