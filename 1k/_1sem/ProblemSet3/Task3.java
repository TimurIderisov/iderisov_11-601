import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task3 {

    public static void main(String[] args) {
        System.out.println("Enter 5 strings");
        Scanner sc = new Scanner(System.in);
        final int LENGTH = 5;
        String[] str = new String[LENGTH];
        Pattern p = Pattern.compile("((01)*(010)*)|((10)*(101)*|1*|0*)");

        for (int i = 0; i < LENGTH; i++) {
            str[i] = sc.nextLine();
        }

        for (int i = 1; i <= LENGTH; i++) {
            Matcher m = p.matcher(str[i - 1]);
            if (m.matches()) {
                System.out.println(i);
            }
        }
    }
}