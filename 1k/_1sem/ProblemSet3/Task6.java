import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task6 {
    public static void main(String[] args) {
        Random r1 = new Random();
        int s = 0;
        int s1 = 0;
        int x;
        Pattern p = Pattern.compile("(2|4|6|8)(0|2|4|6|8)(0|2|4|6|8)(0|2|4|6|8)(0|2|4|6|8)?");
        while (s < 10) {
            x = r1.nextInt(32767);
            Matcher m = p.matcher(x + "");
            if (m.matches()) {
                System.out.println(x);
                s++;
            }

            s1++;
        }
        System.out.println("the number of generated elements: " + s1);
    }
}
