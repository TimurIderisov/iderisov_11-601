/**
* @author Timur Iderisov
* 11-601
* Task16
*/
public class Task16 {
    public static void main(String [] args) {
	    int x = Integer.parseInt(args[0]);
		int y = Integer.parseInt(args[1]);
		int z = Integer.parseInt(args[2]);
		x = ((x + 2) * y - z) / y + y * z;
		System.out.print(x);
    }
}		
		
		