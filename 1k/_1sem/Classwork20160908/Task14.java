/**
* @author Timur Iderisov
* 11-601
* Task14
*/
public class Task14 {
	public static void main(String [] args) {
		int n = Integer.parseInt(args[0]);
		n = n * 10 + 1;
		int s = 1;
		int k = n;
		while(k > 0) {
			k = k / 10;
			s *= 10;
		}
		n = n + s;
		System.out.println(n);
	}
}	