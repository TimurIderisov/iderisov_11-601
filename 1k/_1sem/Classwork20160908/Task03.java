/**
* @author Timur Iderisov
* 11-601
* Task03
*/
public class Task03 {
	public static void main(String [] args) {
		int a = Integer.parseInt(args[0]);
		if (a % 2 == 0 ) {
			System.out.println("EVEN");
		}	
		else {
			System.out.println("ODD");
		}	
	}
}	
	