/**
* @author Timur Iderisov
* 11-601
* Task04
*/
public class Task04 {
    public static void main(String [] args) {
	    double x = Double.parseDouble(args[0]);
		double y = Double.parseDouble(args[1]);
		double z = Double.parseDouble(args[2]);
		double t = x * y * z;
		double s = x + y + z / 2;
		if (t > s) {
		    System.out.println(t * t);
		}	
		else {
			System.out.println(s * s);
		}
	}	
}		