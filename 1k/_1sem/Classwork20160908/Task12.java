/**
* @author Timur Iderisov
* 11-601
* Task12
*/
public class Task12 {
	public static void main(String [] args) {
		int n = Integer.parseInt(args[0]);
		int s = 0;
		while(n > 0) {
			int c = n % 10;
			n = n / 10;
			s = s + c;
		}
		System.out.print(s);
	}	
}	