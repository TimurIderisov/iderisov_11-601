/**
* @author Timur Iderisov
* 11-601
* Task02
*/
public class Task02 {
	public static void main(String [] args) {
		double x = Double.parseDouble(args[0]);
		if(x < -3.5 || x > 0) {
			System.out.println("YES");
		}
		else {
			System.out.println("No");
	 	}
	}
}	