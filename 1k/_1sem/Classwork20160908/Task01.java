/**
* @author Timur Iderisov
* 11-601
* Task01
*/
public class Task01 {
	public static void main(String [] args) {
		double r = Double.parseDouble(args[0]);
		double v = (4 * r * r * r * 3.14) / 3;
		System.out.println(v);
	}
}	
	