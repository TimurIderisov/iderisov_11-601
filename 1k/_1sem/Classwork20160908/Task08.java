/**
* @author Timur Iderisov
* 11-601
* Task08
*/
public class Task08 {
    public static void main(String [] args) {
	    double n = Double.parseDouble(args[0]);
		double s = 0;
		double a;
		for (int x = 2; x <= n; x = x + 2) {
			a = x;
		    s = s + ((a - 1) / a) * ((a - 1) / a) ;
		}	
		System.out.print(s);
	}
}	