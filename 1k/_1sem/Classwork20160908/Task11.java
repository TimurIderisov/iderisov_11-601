/**
* @author Timur Iderisov
* 11-601
* Task11
*/
public class Task11 {
	public static void main(String [] args){
		int n = Integer.parseInt(args[0]);
		for(int x = 1; x <= n; x++) {
		    System.out.println(x + "_" + x * x);
		}	
		for (int x = 1; x <= n; x++) {
		    System.out.print(x + "_____________" );
		}	
		System.out.println();
		for(int x = 1; x <= n; x = x + 1) {
		    System.out.print(x * x + "_____________" );
		}
	}		
}	