/**
* @author Timur Iderisov
* 11-601
* Task13
*/
public class Task13 {
    public static void main(String [] args) {
	    int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		int s = 0;
		while (n > 0) {
		    int c = n % 10;
			n = n / 10;
			if (c == k) {
			    s++;
			}	
		}
		System.out.print(s);
	}	
}	