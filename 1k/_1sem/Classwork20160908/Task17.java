/**
* @author Timur Iderisov
* 11-601
* Task17
*/
public class Task17 {
    public static void main(String [] args) {
	    double x = Double.parseDouble(args[0]);
		x = x * x;
		double x16 = x * x;
		x16 *= x16;
		x16 *= x16;
		double x32 = x16 * x16;
		x = x * x16 * x32;
		System.out.print(x);
	}
}	