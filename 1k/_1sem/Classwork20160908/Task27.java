public class Task27 {
    public static void main(String [] args) {
	    final double EPS = 1e-9;
		int n = 1;
		int x;
		int d = 100;
		int s = 10000;
		int k = 1;
		while (d > EPS) {
		    n = n * (n + 1);
			x =  1 / n;
			d = Math.abs(s - x);
		    s = x;		
			k = k * (-1);
		}
		System.out.print(s);
	}
}