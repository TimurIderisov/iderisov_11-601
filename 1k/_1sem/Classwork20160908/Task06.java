/**
* @author Timur Iderisov
* 11-601
* Task06
*/
public class Task06 {
    public static void main(String [] args) {
	    double a = Double.parseDouble(args[0]);
		double b = Double.parseDouble(args[1]);
		double c = Double.parseDouble(args[2]);
		double d = b * b - 4 * a * c;
		if(d < 0) {
			System.out.println("No solution");
		}
		else {
			if(d == 0) {
				double q = (- b) / (2 * a);
				System.out.println(q);
			}
			else {
				double x = (-b + Math.sqrt(d)) / (2 * a);
				double y = (-b - Math.sqrt(d)) / (2 * a);
				System.out.println(x);
				System.out.println(y);
			}
		}
	}	
}