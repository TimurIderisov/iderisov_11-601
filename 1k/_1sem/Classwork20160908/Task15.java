/**
* @author Timur Iderisov
* 11-601
* Task15
*/
public class Task15 {
	public static void main(String [] args) {
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		int s = 0;
		int l = 1;
		while(n > 0) {
			int c = n % 10;
			n = n / 10;
			if(c != k) {
				s = s + c * l;
				l = l * 10;
			}	
		}
		System.out.print(s);
	}	
}