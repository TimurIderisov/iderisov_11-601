/**
* @author Timur Iderisov
* 11-601
* PS4, Task10
*/
public class RationalComplexNumber {
	RationalFraction rf1 = new RationalFraction();
	RationalFraction rf2 = new RationalFraction();
	
	public RationalComplexNumber() {
	}

	public RationalComplexNumber(RationalFraction rf1, RationalFraction rf2) {
		this.rf1 = rf1;
		this.rf2 = rf2;
	}	
	
	public RationalComplexNumber add(RationalComplexNumber rcn) {
		RationalComplexNumber result = new RationalComplexNumber(rf1.add(rcn.rf1),rf2.add(rcn.rf2));
		return result;
	}
	
	public RationalComplexNumber sub(RationalComplexNumber rcn) {
		RationalComplexNumber result = new RationalComplexNumber(rf1.sub(rcn.rf1),rf2.sub(rcn.rf2));
		return result;
	}
	
	public RationalComplexNumber mult(RationalComplexNumber rcn) {
		RationalComplexNumber result = new RationalComplexNumber(rf1.mult(rcn.rf1),rf2.mult(rcn.rf2));
		return result;
	}
	public String toString() {
		String h;
		String h1;
		h = " ";
		h1 = " ";
		if(rf2.getCh() > 0) {
			h = "+";
			h1 = "i";
		}
		if(rf2.getCh() < 0) {
			h1 = "i";
		}
		String s = rf1 + h + rf2 + h1;
		return s;
	}
	
	public static void main(String [] args) {
		RationalComplexNumber rcn1 = new RationalComplexNumber();
		System.out.println(rcn1);
		
		RationalFraction rf1 = new RationalFraction(1,2);
		RationalFraction rf2 = new RationalFraction(1,4);

		
		RationalComplexNumber rcn2 = new RationalComplexNumber(rf1, rf2);
		System.out.println(rcn2);
		
		RationalComplexNumber rcn3 = rcn2.add(rcn1);
		System.out.println(rcn3);
		
		RationalComplexNumber rcn4 = rcn2.sub(rcn1);
		System.out.println(rcn4);
		
		RationalComplexNumber rcn5 = rcn2.mult(rcn1);
		System.out.println(rcn5);
	}
}