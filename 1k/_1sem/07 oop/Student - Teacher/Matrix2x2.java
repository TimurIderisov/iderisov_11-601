public class Matrix2x2 {
	private double [][] matrix = new double[2][2];
	
	public Matrix2x2(){
		this(0,0,0,0);
	}
	
	public Matrix2x2(double a){
		this(a,a,a,a);
	}
	
	public Matrix2x2(double [][] matrix){
		this.matrix = matrix;
	}
	
	public Matrix2x2(double a,double b,double c,double d){
		matrix[0][0] = a;
		matrix[0][1] = b;
		matrix[1][0] = c;
		matrix[1][1] = d;
	}
	
	public Matrix2x2 add(Matrix2x2 m){
		Matrix2x2 result = new Matrix2x2();
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				result.matrix[i][j] = matrix[i][j] + m.matrix[i][j];
			}
		}
		return result;
	}
	
	public void add2(Matrix2x2 m){
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				matrix[i][j] = matrix[i][j] + m.matrix[i][j];
			}
		}
	}
	
	public Matrix2x2 sub(Matrix2x2 m) {
		Matrix2x2 result = new Matrix2x2();
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				result.matrix[i][j] = matrix[i][j] - m.matrix[i][j];
			}
		}
		return result;
	}
	
	public void sub2(Matrix2x2 m){
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				matrix[i][j] = matrix[i][j] - m.matrix[i][j];
			}
		}
	}
	
	public Matrix2x2 multNumber(double s) {
		Matrix2x2 result = new Matrix2x2();
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				result.matrix[i][j] = matrix[i][j] * s;
			}
		}
		return result;
	}
	
	public void multNumber2(double s) {
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				matrix[i][j] = matrix[i][j] * s;
			}
		}
	}
	
	public Matrix2x2 mult(Matrix2x2 m){
		double c;
		Matrix2x2 result = new Matrix2x2();
		for (int i=0;i<matrix.length;i++){
			for (int j=0;j<matrix[i].length;j++){
				c = 0;
				for (int k=0;k<matrix.length;k++){
					c += matrix[i][k]*m.matrix[k][j];
					
				}
				result.matrix[i][j] = c;
			}
		}
		return result;
	}
	
	public void mult2(Matrix2x2 m){
		double c;
		for (int i=0;i<matrix.length;i++){
			for (int j=0;j<matrix[i].length;j++){
				c = 0;
				for (int k=0;k<matrix.length;k++){
					c += matrix[i][k]*m.matrix[k][j];
					
				}
				matrix[i][j] = c;
			}
		}
	}
	
	public double det() {
		return matrix[0][0]*matrix[1][1] -
			matrix[0][1]*matrix[1][0];
	}
	
	
	public Matrix2x2 getInverse() {
		Matrix2x2 result = new Matrix2x2();
		double det = det();
		result.matrix[0][0] = matrix[1][1] / det;
		result.matrix[0][1] = -1 * matrix[0][1] / det;
		result.matrix[1][0] = -1 * matrix[1][0] / det;
		result.matrix[1][1] = matrix[0][0] / det;
		return result;
	}
	
	public void transpon() {
		double c;
		Matrix2x2 result = new Matrix2x2();
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				result.matrix[i][j] = matrix[j][i];
			}
		}
		
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				matrix[i][j] = result.matrix[i][j];
			}
		}
	}
	
	public String toString(){
		String s = "";
		for (int i=0;i<matrix.length;i++){
			for (int j=0;j<matrix[i].length;j++){
				s += matrix[i][j] + " ";
			}
			s += "\n";
		}
		return s;
	}
	
	
	
	
	
	public static void main(String [] args) {
		
	Matrix2x2 m1 = new Matrix2x2(1,2,3,4);	
	Matrix2x2 m2 = new Matrix2x2(new double[][] {
		{1,0},
		{0,1}
	});
	System.out.println(m1);
	
	System.out.println(m2);
	
	Matrix2x2 m6 = m1.add(m2);
	System.out.println(m6);
	
	Matrix2x2 m7 = m1.sub(m2);
	System.out.println(m7);
	
	Matrix2x2 m8 = m1.multNumber(10);
	System.out.println(m8);
	
	m1.transpon();
	System.out.println(m1);
	
	Matrix2x2 m4 = m1.getInverse();
	System.out.println(m4);
	
	Matrix2x2 m5 = m1.mult(m4);
	System.out.println(m5);
	}
	
}