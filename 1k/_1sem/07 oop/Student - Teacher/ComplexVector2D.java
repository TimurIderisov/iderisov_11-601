public class ComplexVector2D {
    ComplexNumber cn1 = new ComplexNumber();
	ComplexNumber cn2 = new ComplexNumber();
	
	public ComplexVector2D() {
	}
	
	public ComplexVector2D(ComplexNumber rcn1, ComplexNumber rcn2) {
		this.cn1 = rcn1;
		this.cn2 = rcn2;
	}
	
	public ComplexNumber getcn1() {
		return cn1;
	}
	
	public ComplexNumber getcn2() {
		return cn2;
	}
	
	public ComplexVector2D add(ComplexVector2D rv) {
		ComplexVector2D result = new ComplexVector2D(cn1.add(rv.getcn1()), cn2.add(rv.getcn2()));
		return result;
	}
	
	public String toString() {
		return "<" + cn1 + ", " + cn2 + ">";
	}
	
	public ComplexNumber scalarProduct(ComplexVector2D rv) {
		ComplexNumber result = (cn1.mult(rv.getcn1())).add(cn2.mult(rv.getcn2()));
		return result;
	}
	
	public boolean equals(ComplexVector2D rv) {
		boolean flag = false;
		if(cn1.equals(rv.getcn1()) == true && cn2.equals(rv.getcn2()) == true){
			flag = true;
		}
		return flag;
	}
	
	public static void main(String [] args) {
		ComplexNumber cn1 = new ComplexNumber();
		
		ComplexNumber cn2 = new ComplexNumber(6, 8);
		
		
		
		ComplexVector2D rv1 = new ComplexVector2D();
		
		ComplexVector2D rv2 = new ComplexVector2D(cn1,cn2);
		
		System.out.println(rv1);
		System.out.println(rv2);
		
		ComplexVector2D rv3 = rv1.add(rv2);
		ComplexNumber cnn12 = rv1.scalarProduct(rv2);
		
		System.out.println(rv3);
		System.out.println(cnn12);
		
		
		System.out.println(rv1.equals(rv2));
	}
	
}