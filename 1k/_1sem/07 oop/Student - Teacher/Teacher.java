import java.util.Random;
public class Teacher {
	String fio;
	String pred;
	
	public Teacher(String fio, String pred) {
		this.fio = fio;
		this.pred = pred;
	}
	
	public String getFio() {
		return fio;
		}
	public String getPred() {
		return pred;
	}
	
	public void setFio(String s) {
		fio = s;
	}
	public void setPred(String s) {
		pred = s;
	}
	
	public String check(Student st) {
		String b = new String();
		double a = (Math.random() * 4);
		if(a < 1) {
			b = "mark A";
		}
		if(a >= 1 && a < 2) {
			b = "mark B";
		}
		if(a < 3 && a >= 2) {
			b = "mark C";
		}
		if(a >= 3) {
			b = "mark D";
		}
		return " teacher " + getFio() +  " voted student named " + st.getName() + " by subject " + getPred() + " to the " + b ;
		}
}