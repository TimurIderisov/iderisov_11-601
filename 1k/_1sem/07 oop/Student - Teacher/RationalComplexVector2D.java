/**
* @author Timur Iderisov
* 11-601
* PS4, Task11
*/
public class RationalComplexVector2D {
	RationalComplexNumber rcn1 = new RationalComplexNumber();
	RationalComplexNumber rcn2 = new RationalComplexNumber();
	
	public RationalComplexVector2D() {
	}
	
	public RationalComplexVector2D(RationalComplexNumber rcn1, RationalComplexNumber rcn2) {
		this.rcn1 = rcn1;
		this.rcn2 = rcn2;
	}
	
	public RationalComplexVector2D add(RationalComplexVector2D rcv) {
		RationalComplexVector2D result = new RationalComplexVector2D(rcn1.add(rcv.rcn1), rcn2.add(rcv.rcn2));
		return result;
	}
	
	public RationalComplexNumber scalarProduct(RationalComplexVector2D rcv) {
		RationalComplexNumber result =(rcn1.mult(rcv.rcn1)).add(rcn2.add(rcv.rcn2));
		return result;
	}
	
	public String toString() {
		return "<" + rcn1 + "," +  rcn2 +">";
	}
	
	public static void main(String [] args) {
		RationalFraction rf1 = new RationalFraction();
		RationalFraction rf2 = new RationalFraction(4,9);
		
		RationalComplexNumber rcn3 = new RationalComplexNumber();
		RationalComplexNumber rcn4= new RationalComplexNumber(rf1,rf2);
		
		RationalComplexVector2D rcv5 = new RationalComplexVector2D();
		RationalComplexVector2D rcv6 = new RationalComplexVector2D(rcn3, rcn4);
		
		System.out.println(rcv5);
		System.out.println(rcv6);
		
		RationalComplexVector2D rcv3 = rcv5.add(rcv6);
		System.out.println(rcv3);
		
		RationalComplexNumber rcn77 = rcv5.scalarProduct(rcv6);
		System.out.println(rcn77);
	}
}