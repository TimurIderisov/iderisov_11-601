/**
* @author Timur Iderisov
* 11-601
* PS4, Task02
*/
public class Vector2D {
	private double x, y;
	public static double l;
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}	
	
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2D() {
	}
	
	public Vector2D add(Vector2D v) {
		Vector2D result = new Vector2D(x + v.getX(), y + v.getY());
		return result;
	}
	
	public void add2(Vector2D v2) {
			this.x = x + v2.getX();
			this.y = y + v2.getY();
		}
		
	public Vector2D sub(Vector2D v) {
		Vector2D result = new Vector2D(x - v.getX(), y - v.getY());
		return result;
	}
	
	public void sub2(Vector2D v2) {
			this.x = x - v2.getX();
			this.y = y - v2.getY();
		}
	
	public Vector2D mult(double a) {
		Vector2D result = new Vector2D(x * a, y * a);
		return result;
	}
	
	public void mult2(double a) {
		this.x = x * a;
		this.y = y * a;
	}
		
	public String toString() {
		return "<" + x + ", " + y + ">";
	}
	
	
	public boolean equals(Vector2D v2) {
		boolean flag = false;
		if(Math.sqrt(x*x + y*y) == v2.getLength()) {
		flag = true;
		}
		return flag;
	}

	
	public double scalarProduct(Vector2D v) {
		double result = x * v.getX() + y * v.getY();
		return result;
	}
	
	public double getLength() {
		l = Math.sqrt(x * x + y * y);
		return l;
	}
	
	public static double cosOfAngel(Vector2D v, Vector2D c) {
		l = (v.getX() * c.getX() + v.getY() * c.getY()) /( v.getLength() * c.getLength());
		return l;
	}
	
	public static void main(String [] args) {
		double a = 4;
	
		Vector2D v0 = new Vector2D();
		Vector2D v1 = new Vector2D(1,2);
		Vector2D v2 = new Vector2D(3,1);
		Vector2D v3 = v1.add(v2);
		System.out.println(v3);
		double scv = v1.scalarProduct(v2);
		System.out.println(scv);
		System.out.println(v3.getLength());
		System.out.println(Vector2D.cosOfAngel(v3,v2));
		v1.add2(v2);
		System.out.println(v1);
		Vector2D v5 = v1.sub(v2);
		System.out.println(v5);
		v5.sub2(v2);
		System.out.println(v5);
		Vector2D v6 = v1.mult(a);
		System.out.println(v6);
		v6.mult2(a);
		System.out.println(v6);
		System.out.println(v3.equals(v2));
	}
}