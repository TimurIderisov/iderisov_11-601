/**
* @author Timur Iderisov
* 11-601
* PS4, Task09
*/
public class ComplexMatrix2x2 {
	private ComplexNumber [][] cmatrix = new ComplexNumber[2][2];
	
	public ComplexMatrix2x2(){
	}
	
	public ComplexMatrix2x2(ComplexNumber rf) {
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				cmatrix[i][j] = rf;
			}
		}
	}
	
	public ComplexMatrix2x2(ComplexNumber rf, ComplexNumber rf1, ComplexNumber rf2, ComplexNumber rf3) {
		cmatrix[0][0] = rf;
		cmatrix[0][1] = rf1;
		cmatrix[1][0] = rf2;
		cmatrix[1][1] = rf3;
	}
	
	public ComplexMatrix2x2 add(ComplexMatrix2x2 rm) {
		ComplexMatrix2x2 result = new ComplexMatrix2x2();
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				result.cmatrix[i][j] = (cmatrix[i][j]).add(rm.cmatrix[i][j]);
			}
		}
		return result;
	}
	
	public ComplexMatrix2x2 mult(ComplexMatrix2x2 m){
		ComplexNumber c = new ComplexNumber();
		ComplexMatrix2x2 result = new ComplexMatrix2x2();
		for (int i=0;i<2;i++){
			for (int j=0;j<2;j++){
				c = new ComplexNumber();
				for (int k=0;k<cmatrix.length;k++){
					c = c.add((cmatrix[i][k]).mult(m.cmatrix[k][j]));
				}
				result.cmatrix[i][j] = c;
			}
		}
		return result;
	}
	
	public ComplexNumber det() {
		ComplexNumber result = new ComplexNumber();
		result = ((cmatrix[0][0]).mult(cmatrix[1][1])).sub((cmatrix[0][1]).mult(cmatrix[1][0]));
		return result;
	}
	
	public ComplexVector2D multVector(ComplexVector2D rv) {
		ComplexVector2D result = new ComplexVector2D(rv.getcn1().mult((cmatrix[0][0]).add(cmatrix[1][0])),rv.getcn2().mult((cmatrix[1][0]).add(cmatrix[1][1])));
		return result;
	}	
	
	
	public String toString() {
		String s = "";
		for (int i = 0; i <= 1; i++) {
			for (int j = 0; j <= 1;j++) {
				s += cmatrix[i][j] + " ";
			}
			s += "\n";
		}
		return s;
	}
	
	public static void main(String [] args) {
		ComplexNumber rf1 = new ComplexNumber(6, 8);
		ComplexVector2D rv1 = new ComplexVector2D(rf1, rf1);
	
		ComplexMatrix2x2 rm1 = new ComplexMatrix2x2(rf1);
		ComplexMatrix2x2 rm2 = new ComplexMatrix2x2(rf1,rf1,rf1,rf1);
		ComplexMatrix2x2 rm3 = new ComplexMatrix2x2();
		
		ComplexMatrix2x2 rm4 = rm1.add(rm2);
		ComplexMatrix2x2 rm5 = rm1.mult(rm2);
		ComplexNumber rf0 = rm1.det();
		ComplexVector2D rv0 = rm1.multVector(rv1);
		
		System.out.println(rm1);
		System.out.println(rm2);
		System.out.println(rm3);
		System.out.println(rm4);
		System.out.println(rm5);
		System.out.println(rf0);
		System.out.println(rv0);
	}
}
