public class ComplexNumber {
	double a;
	double b;
	public String h1;
	public String h;
	public ComplexNumber() {
		this.a = 0;
		this.b = 0;
	}
	
	public ComplexNumber(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	public double getA() {
		return a;
	}
	
	public double getB() {
		return b;
	}
	
	public ComplexNumber add(ComplexNumber c) {
		ComplexNumber result = new ComplexNumber(a + c.getA(), b + c.getB());
		return result;
	}
	
	void add2(ComplexNumber c) {
		this.a = a + c.getA();
		this.b = b + c.getB();
	}
	
	public ComplexNumber sub(ComplexNumber c) {
		ComplexNumber result = new ComplexNumber(a - c.getA(), b - c.getB());
		return result;
	}
	
	void sub2(ComplexNumber c) {
		this.a = a - c.getA();
		this.b = b - c.getB();
	}
	
	public ComplexNumber multNumber(double t) {
		ComplexNumber result = new ComplexNumber(a * t, b * t);
		return result;
	}
	
	void multNumber2(double t) {
		this.a = a * t;
		this.b = b * t;
	}
	
	public ComplexNumber mult(ComplexNumber c) {
		ComplexNumber result = new ComplexNumber(a * c.getA() + b * c.getB(), a * c.getB() + b * c.getA());
		return result;
	}
	
	public void mult2(ComplexNumber c) {
		this.a = a * c.getA() + b * c.getB();
		this.b = a * c.getB() + b * c.getA();
	}
	
	public ComplexNumber div(ComplexNumber c) {
		ComplexNumber result = new ComplexNumber((a * c.getA() + b * c.getB())/(a * a + c.getB() * c.getB()),( b * c.getA() - a * c.getB()) / (c.getA() * c.getA() + c.getB() * c.getB()));
		return result;
	}
	
	public void div2(ComplexNumber c) {
		this.a = (a * c.getA() + b * c.getB()) / (a * a + c.getB() * c.getB());
		this.b = (b * c.getA() - a * c.getB()) / (c.getA() * c.getA() + c.getB() * c.getB());
	}	
		
	public double length() {
		double l = Math.sqrt(a * a + b * b);
		return l;
	}	
	
	public String toString() {
		h = "";
		h1 = "";
		if(b > 0) {
			h = "+";
			h1 = "i";
		}
		if(b < 0) {
			h1 = "i";
		}
		String s = a + h + b + h1;
		return s;
	}
	
	public double arg() {
		double result = Math.atan(b / a);
		return result;
	}
	
	public ComplexNumber pow(double n) {
		ComplexNumber result = new ComplexNumber(Math.pow(length(),n) * Math.cos(n * arg()), Math.pow(length(),n) * Math.sin(n * arg()) );
		return result;
	}
	
	public boolean equals(ComplexNumber c) {
		boolean flag = false;
		if(a == c.getA() && b == c.getB()) {
			flag = true;
		}
		return flag;
	}
	
	public static void main(String [] args) {
		ComplexNumber c1 = new ComplexNumber();
		System.out.println(c1);
		
		ComplexNumber c2 = new ComplexNumber(2, 4);
		System.out.println(c2);
		
		ComplexNumber c3 = c1.add(c2);
		System.out.println(c3);
		
		c1.add2(c2);
		System.out.println(c1);
		
		c3 = c1.sub(c2);
		System.out.println(c3);
		
		c1.sub2(c2);
		System.out.println(c1);
		
		ComplexNumber c4 = c1.multNumber(5);
		System.out.println(c4);
		
		c4.multNumber2(5);
		System.out.println(c4);
		
		c4 = c1.mult(c2);
		System.out.println(c4);
		
		c4.mult2(c2);
		System.out.println(c4);
		
		c4 = c1.div(c2);
		System.out.println(c4);
		
		c4.div2(c2);
		System.out.println(c4);
		
		c4 = c1.pow(2);
		System.out.println(c4);
		
		boolean kl = c1.equals(c2);
		System.out.println(kl);
		
	}
}