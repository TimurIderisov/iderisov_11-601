public class RationalVector2D {
	RationalFraction r1 = new RationalFraction();
	RationalFraction r2 = new RationalFraction();
	
	public RationalVector2D() {
	}
	
	public RationalVector2D(RationalFraction rr1, RationalFraction rr2) {
		this.r1 = rr1;
		this.r2 = rr2;
	}
	
	public RationalFraction getR1() {
		return r1;
	}
	
	public RationalFraction getR2() {
		return r2;
	}
	
	public RationalVector2D add(RationalVector2D rv) {
		RationalVector2D result = new RationalVector2D(r1.add(rv.getR1()), r2.add(rv.getR2()));
		return result;
	}
	
	public String toString() {
		return "<" + r1 + ", " + r2 + ">";
	}
	
	double length() {
		RationalFraction r11 = (r1.mult(r1)).add(r2.mult(r2));
		double l = Math.sqrt(r11.value());
		return l;
	}
	
	public RationalVector2D scalarProduct(RationalVector2D rv) {
		RationalVector2D result = new RationalVector2D(r1.mult(rv.getR1()), r2.mult(rv.getR2()));
		return result;
	}
	
	public boolean equals(RationalVector2D rv) {
		boolean flag = false;
		if(r1.equals(rv.getR1()) == true && r2.equals(rv.getR2()) == true){
			flag = true;
		}
		return flag;
	}
	
	public static void main(String [] args) {
		RationalFraction r1 = new RationalFraction();
		
		RationalFraction r2 = new RationalFraction(6, 8);
		
		
		
		RationalVector2D rv1 = new RationalVector2D();
		
		RationalVector2D rv2 = new RationalVector2D(r1,r2);
		
		System.out.println(rv1);
		System.out.println(rv2);
		
		RationalVector2D rv3 = rv1.add(rv2);
		
		RationalVector2D rv4 = rv1.scalarProduct(rv2);
		System.out.println(rv3);
		System.out.println(rv4);
		
		System.out.println(rv2.length());
		
		System.out.println(rv1.equals(rv2));
	}
	
}