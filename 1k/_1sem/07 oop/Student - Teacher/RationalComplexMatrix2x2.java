/**
* @author Timur Iderisov
* 11-601
* PS4, Task12
*/
public class RationalComplexMatrix2x2 {
	
	private RationalComplexNumber [][] rcnmatrix = new RationalComplexNumber [2][2];
	
	RationalComplexMatrix2x2(){
	}
	
	RationalComplexMatrix2x2(RationalComplexNumber rcn) {
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				rcnmatrix[i][j] = rcn;
			}
		}
	}
	
	RationalComplexMatrix2x2(RationalComplexNumber rcn1, RationalComplexNumber rcn2, RationalComplexNumber rcn3, RationalComplexNumber rcn4) {
		rcnmatrix[0][0] = rcn1;
		rcnmatrix[0][1] = rcn2;
		rcnmatrix[1][0] = rcn3;
		rcnmatrix[1][1] = rcn4;
	}
	
	RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 rcnm) {
		RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				result.rcnmatrix[i][j] = (rcnmatrix[i][j]).add(rcnm.rcnmatrix[i][j]);
			}
		}
		return result;
	}
	
	RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 rcnm) {
		RationalComplexNumber c = new RationalComplexNumber();
		RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
		for (int i=0;i<2;i++){
			for (int j=0;j<2;j++){
				c = new RationalComplexNumber();
				for (int k=0;k<2;k++){
					c = c.add((rcnmatrix[i][k]).mult(rcnm.rcnmatrix[k][j]));
				}
				result.rcnmatrix[i][j] = c;
			}
		}
		return result;
	}
	
	RationalComplexNumber det() {
		RationalComplexNumber result = new RationalComplexNumber();
		result = ((rcnmatrix[0][0]).mult(rcnmatrix[1][1])).sub((rcnmatrix[0][1]).mult(rcnmatrix[1][0]));
		return result;
	}
	
	RationalComplexVector2D multVector(RationalComplexVector2D rcv) {
		RationalComplexVector2D result = new RationalComplexVector2D((rcv.rcn1).mult((rcnmatrix[0][0]).add(rcnmatrix[1][0])),(rcv.rcn2).mult((rcnmatrix[1][0]).add(rcnmatrix[1][1])));
		return result;
	}
	
	public static void main(String [] args) {
		
		RationalComplexNumber rcn1 = new RationalComplexNumber();
		System.out.println(rcn1);
		
		RationalFraction rf1 = new RationalFraction(1,2);
		RationalFraction rf2 = new RationalFraction(1,4);
		
		RationalComplexNumber rcn3 = new RationalComplexNumber();
		RationalComplexNumber rcn4= new RationalComplexNumber(rf1,rf2);
		
		RationalComplexVector2D rcv12 = new RationalComplexVector2D(rcn3, rcn4);

		RationalComplexNumber rcn2 = new RationalComplexNumber(rf1, rf2);
		
		RationalComplexMatrix2x2 rcm1 = new RationalComplexMatrix2x2();
		RationalComplexMatrix2x2 rcm2 = new RationalComplexMatrix2x2(rcn2);
		RationalComplexMatrix2x2 rcm3 = new RationalComplexMatrix2x2(rcn2, rcn2, rcn2, rcn2);

		
		RationalComplexMatrix2x2 rcm77 = rcm1.add(rcm2);
		System.out.println(rcm77);
		
		RationalComplexMatrix2x2 rcm4 = rcm1.mult(rcm3);
		System.out.println(rcm4);
		
		RationalComplexNumber rcn66 = rcm2.det();
		System.out.println(rcn66);
		
		RationalComplexVector2D rcv5 = rcm1.multVector(rcv12);
		System.out.println(rcv12);
		
		
	}
}