/**
* @author Timur Iderisov
* 11-601
* PS4, Task08
*/
public class RationalMatrix2x2 {
	private RationalFraction [][] rmatrix = new RationalFraction[2][2];
	
	public RationalMatrix2x2(){
	}
	
	public RationalMatrix2x2(RationalFraction rf) {
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				rmatrix[i][j] = rf;
			}
		}
	}
	
	public RationalMatrix2x2(RationalFraction rf, RationalFraction rf1, RationalFraction rf2, RationalFraction rf3) {
		rmatrix[0][0] = rf;
		rmatrix[0][1] = rf1;
		rmatrix[1][0] = rf2;
		rmatrix[1][1] = rf3;
	}
	
	public RationalMatrix2x2 add(RationalMatrix2x2 rm) {
		RationalMatrix2x2 result = new RationalMatrix2x2();
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				result.rmatrix[i][j] = (rmatrix[i][j]).add(rm.rmatrix[i][j]);
			}
		}
		return result;
	}
	
	public RationalMatrix2x2 mult(RationalMatrix2x2 m){
		RationalFraction c = new RationalFraction();
		RationalMatrix2x2 result = new RationalMatrix2x2();
		for (int i=0;i<rmatrix.length;i++){
			for (int j=0;j<rmatrix[i].length;j++){
				c = new RationalFraction();
				for (int k=0;k<rmatrix.length;k++){
					c = c.add((rmatrix[i][k]).mult(m.rmatrix[k][j]));
					
				}
				result.rmatrix[i][j] = c;
			}
		}
		return result;
	}
	
	public RationalFraction det() {
		RationalFraction result = new RationalFraction();
		result = ((rmatrix[0][0]).mult(rmatrix[1][1])).sub((rmatrix[0][1]).mult(rmatrix[1][0]));
		return result;
	}
	
	public RationalVector2D multVector(RationalVector2D rv) {
		RationalVector2D result = new RationalVector2D(rv.getR1().mult((rmatrix[0][0]).add(rmatrix[1][0])),rv.getR2().mult((rmatrix[1][0]).add(rmatrix[1][1])));
		return result;
	}	
	
	
	public String toString() {
		String s = "";
		for (int i = 0; i <= 1; i++) {
			for (int j = 0; j <= 1;j++) {
				s += rmatrix[i][j] + " ";
			}
			s += "\n";
		}
		return s;
	}
	
	public static void main(String [] args) {
		RationalFraction rf1 = new RationalFraction(6, 8);
		RationalVector2D rv1 = new RationalVector2D(rf1, rf1);
	
		RationalMatrix2x2 rm1 = new RationalMatrix2x2(rf1);
		RationalMatrix2x2 rm2 = new RationalMatrix2x2(rf1,rf1,rf1,rf1);
		RationalMatrix2x2 rm3 = new RationalMatrix2x2();
		
		RationalMatrix2x2 rm4 = rm1.add(rm2);
		RationalMatrix2x2 rm5 = rm1.mult(rm2);
		RationalFraction rf0 = rm1.det();
		RationalVector2D rv0 = rm1.multVector(rv1);
		
		System.out.println(rm1);
		System.out.println(rm2);
		System.out.println(rm3);
		System.out.println(rm4);
		System.out.println(rm5);
		System.out.println(rf0);
		System.out.println(rv0);
	}
}



