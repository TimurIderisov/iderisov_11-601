public class Student {
	String name;
	int group;
	
	public Student(String name, int group) {
		this.name = name;
		this.group = group;
	}

	public String getName(){
		return name;
	}
	
	public double getGroup(){
		return group;
	}	
	
	public void setName(String s){
		name = s;
	}
	
	public void setGroup(int n){
		group = n;
	}	
	
	
}