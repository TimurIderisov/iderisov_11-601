public class RationalFraction {
	
	double l;
	int cel;
	
	public int ch;
	public int zn;
	
	public RationalFraction() {
		this.ch = 0;
		this.zn = 0;
	}
	
	public RationalFraction(int ch, int zn) {
		this.ch = ch;
		this.zn = zn;
	}
	
	public int getCh() {
		return ch;
	}
	
	public int getZn() {
		return zn;
	}
	
	void reduce() {
		int max;
		if(ch > zn) {
			max = ch;
		}
		else {
			max = zn;
		}
		for(int i = max; i >= 1; i = i - 1) {
			if(zn % i == 0 && ch % i == 0) {
				zn = zn / i;
				ch = ch / i;
			}
		}
	}
	
	public RationalFraction add(RationalFraction dr) {
		RationalFraction result = new RationalFraction(ch * dr.getZn() + dr.getCh() * zn,zn * dr.getZn());
		result.reduce();
		return result;
		
	} 
	
	void add2(RationalFraction dr){
		this.ch = ch + dr.getCh();
		this.zn = zn + dr.getZn();
		dr.reduce();
	}
	
	public RationalFraction sub(RationalFraction dr) {
		RationalFraction result = new RationalFraction(ch - dr.getCh(),zn - dr.getZn());
		result.reduce();
		return result;
	}
	
	void sub2(RationalFraction dr){
		this.ch = ch - dr.getCh();
		this.zn = zn - dr.getZn();
		dr.reduce();
	}
	
	public RationalFraction mult(RationalFraction dr) {
		RationalFraction result = new RationalFraction(ch * dr.getCh(), zn * dr.getZn());   
		result.reduce();
		return result;
	}
	
	void mult2(RationalFraction dr) {
		this.zn = zn * dr.getZn();
		this.ch = ch * dr.getCh();
		dr.reduce();
	}
	
	public RationalFraction div(RationalFraction dr) {
		RationalFraction result = new RationalFraction(ch * dr.getZn(), zn * dr.getCh());
		result.reduce();
		return result;
	}
	
	void div2(RationalFraction dr) {
		this.zn = zn * dr.getCh();
		this.ch = ch * dr.getZn();
		dr.reduce();
	}
	
	public String toString() {
		return ch + "/" + zn;
	}
	
	public double value() {
		l = ch / zn;
		return l;
	}
	
	public boolean equals(RationalFraction dr) {
	
		boolean flag = false;
		if((ch == dr.getCh()) && (zn == dr.getZn())){
			flag = true;
		}
		else {
			flag = false;
		}
		return flag;
	}
	
	public int numberPart() {
		cel = ch / zn;
		return cel;
	}
	
	public static void main(String [] args) {
		RationalFraction r1 = new RationalFraction();
		System.out.println(r1);
		
		RationalFraction r2 = new RationalFraction(6, 8);
		System.out.println(r2);
		
		r2.reduce();
		System.out.println(r2);
		System.out.println(r2.value());
		
		
		System.out.println(r2);
		
		System.out.println(r2.add(r1));
		
		r2.add2(r1);
		
		System.out.println(r1.sub(r2));
		
		r2.sub2(r1);
		
		System.out.println(r2);
		
		System.out.println(r2.mult(r1));
		
		r2.mult2(r1);
		
		System.out.println(r1);
		
		System.out.println(r2.div(r1));
		
		r2.div2(r1);
		
		System.out.println(r2);
		
		System.out.println(r2.equals(r1));
	}
	
	
}