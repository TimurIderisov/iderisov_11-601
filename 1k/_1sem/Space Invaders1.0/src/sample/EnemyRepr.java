package sample;

import javafx.scene.Group;
import javafx.scene.shape.Circle;

public class EnemyRepr {
    private int radius = 10;
    private Circle circle;
    private Enemy enemy;

    public EnemyRepr(Group root, Enemy en) {
        this.enemy = en;
        circle = new Circle(en.getX(),en.getY(),radius);
        circle.setStyle("-fx-fill:red");
        root.getChildren().add(circle);
    }
    public void del(Group root) {
        root.getChildren().remove(circle);
    }

    public void updateY(){circle.setCenterY(enemy.getY());
    }
}
