package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class Main extends Application {
    private final static int WIDTH = 400;
    private final static int HEIGHT = 400;
    private Player player;
    private Scene root;
    private Enemy[] enemies;
    private Bullet bullet;
    private Label l123 = new Label();
    private int points = 0;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        player = new Player(WIDTH / 2, HEIGHT - 30, root);
        enemies = new Enemy[1000];
        final int[] enemies_number = {0};
        new AnimationTimer() {
            long was = System.currentTimeMillis();

            @Override
            public void handle(long now) {
                for (int i = 0; i < enemies_number[0]; i++) {
                    enemies[i].setY();
                }
                if (System.currentTimeMillis() - was > 1000) {
                    enemies[enemies_number[0]] = new Enemy(root);
                    enemies_number[0]++;
                    was = System.currentTimeMillis();
                }
                for (int i = 0; i < enemies_number[0]; i++) {
                    if (enemies[i].getY() > WIDTH - 70) {
                        enemies[i].er.del(root);
                    }
                }


            }

        }.start();


        root.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case RIGHT:
                    player.updateX(1);
                    break;
                case LEFT:
                    player.updateX(-1);
                    break;
                case SPACE:
                    Bullet bullet = new Bullet(player.getX(), player.getY(), root);

                    new AnimationTimer() {

                        @Override
                        public void handle(long now) {
                            bullet.setY(bullet.getY() - 3);
                            if (bullet.getY() < 20)
                                bullet.br.del(root);
                            for (int i = 0; i < enemies_number[0]; i++) {
                                if (Math.sqrt((bullet.getY() - enemies[i].getY()) * (bullet.getY() - enemies[i].getY()) + (bullet.getX() - enemies[i].getX()) * (bullet.getX() - enemies[i].getX())) <= 13) {
                                    enemies[i].er.del(root);
                                    bullet.br.del(root);
                                    points++;
                                    l123.setText("POINTS : " + points);
                                }
                            }
                        }


                    }.start();

                    break;
            }
        });
        root.getChildren().add(l123);
        primaryStage.setTitle("Space Invaders");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
        root.requestFocus();
    }


    public static void main(String[] args) {
        launch(args);

    }
}
