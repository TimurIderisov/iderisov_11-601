package sample;

import javafx.scene.Group;
import javafx.scene.shape.Circle;

public class BulletRepr {
    private int radius = 3;
    private Bullet bullet;
    private Circle circle;

    public  BulletRepr(Group root, Bullet b){
        this.bullet = b;
        circle = new Circle(b.getX(),b.getY(),radius);
        root.getChildren().add(circle);
    }

    public void del(Group root){
        root.getChildren().remove(circle);
    }

    public void updateY(){circle.setCenterY(bullet.getY());
    }
}
