package sample;

import javafx.scene.Group;

public class Player {
    private int x, y, v, hp, scores;
    private PlayerRepr pr;

    public void updateX(int dir) {
        this.x = this.x + dir * v;
        pr.updateX();
    }

    public Player(int x, int y, Group group) {
        this.x = x;
        this.y = y;
        this.v = 10;
        this.hp = 100;
        this.scores = 0;
        pr = new PlayerRepr(group, this);
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
