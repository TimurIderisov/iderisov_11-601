package sample;

import javafx.scene.Group;

public class Enemy {
    private int x;
    private int y = 0;
    public EnemyRepr er;
    private int WID = 400;

    public Enemy(Group root) {
        x = (int) (WID * Math.random());
        er = new EnemyRepr(root, this);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setY() {
        y++;
        er.updateY();
    }
}
