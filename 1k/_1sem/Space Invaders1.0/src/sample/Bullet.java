package sample;

import javafx.scene.Group;

public class Bullet {
    private int x, y;
    public BulletRepr br;

    public Bullet(int x, int y, Group root) {
        this.x = x;
        this.y = y;
        br = new BulletRepr(root, this);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        br.updateY();
    }
}
