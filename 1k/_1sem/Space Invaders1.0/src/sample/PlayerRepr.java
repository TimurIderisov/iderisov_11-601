package sample;

import javafx.scene.Group;
import javafx.scene.shape.Circle;

public class PlayerRepr {
    private int radius = 15;
    private Circle circle;
    private Player player;

    public PlayerRepr(Group root, Player p) {
        this.player = p;
        circle = new Circle(p.getX(), p.getY(), radius);
        root.getChildren().add(circle);
    }

    public void updateX() {
        circle.setCenterX(player.getX());
    }
}
