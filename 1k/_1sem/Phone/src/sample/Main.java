package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        final int[] ost = {0};
        final String[] res = {""};

        Group root = new Group();
        Button b1 = new Button("ABC");
        b1.setLayoutX(20);
        b1.setLayoutY(50);
        Button b2 = new Button("DEF");
        b2.setLayoutX(70);
        b2.setLayoutY(50);
        Button b3 = new Button("GHI");
        b3.setLayoutX(120);
        b3.setLayoutY(50);

        Button b4 = new Button("JKL");
        b4.setLayoutX(20);
        b4.setLayoutY(80);
        Button b5 = new Button("MNO");
        b5.setLayoutX(70);
        b5.setLayoutY(80);
        Button b6 = new Button("PQR");
        b6.setLayoutX(120);
        b6.setLayoutY(80);

        Button b7 = new Button("STU");
        b7.setLayoutX(20);
        b7.setLayoutY(120);
        Button b8 = new Button("VWX");
        b8.setLayoutX(70);
        b8.setLayoutY(120);
        Button b9 = new Button("YZ-");
        b9.setLayoutX(120);
        b9.setLayoutY(120);
        Button b10 = new Button("ADD");
        b10.setLayoutY(6);
        b10.setLayoutX(150);

        Label label = new Label();
        label.setLayoutX(30);
        label.setLayoutY(25);
        Label lab = new Label();
        lab.setLayoutX(120);
        lab.setLayoutY(10);

        root.getChildren().addAll(b1, b2, b3, b4, b5, b6, b7, b8, b9, label, lab, b10);
        primaryStage.setScene(new Scene(root, 200, 175));
        primaryStage.show();

        push(b1, b10, label, lab, "A", "B", "C");
        push(b2, b10, label, lab, "D", "E", "F");
        push(b3, b10, label, lab, "G", "H", "I");
        push(b4, b10, label, lab, "J", "K", "L");
        push(b5, b10, label, lab, "M", "N", "O");
        push(b6, b10, label, lab, "P", "Q", "R");
        push(b7, b10, label, lab, "S", "T", "U");
        push(b8, b10, label, lab, "V", "W", "X");
        push(b9, b10, label, lab, "Y", "Z", "_");


    }

    public void push(Button bE, Button b11, Label labelE, Label labE, String a, String b, String c) {
        final int[] ostE = {0};
        final String[] resE = {""};

        bE.setOnAction(event -> {
            if (ostE[0] % 3 == 0) {
                resE[0] = a;
            }
            if (ostE[0] % 3 == 1) {
                resE[0] = b;
            }
            if (ostE[0] % 3 == 2) {
                resE[0] = c;
            }
            labE.setText(resE[0]);
            ostE[0]++;
            b11.setOnAction(event1 -> {
                labelE.setText(labelE.getText() + resE[0]);
                resE[0] = "";
                ostE[0] = 0;
                labE.setText("");
            });
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}

