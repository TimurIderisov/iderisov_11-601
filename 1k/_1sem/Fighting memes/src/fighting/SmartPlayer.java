package fighting;
public class SmartPlayer extends Player { 
	private int healPoints;
	
	public SmartPlayer(String name, String battleCry) {
		super(name, battleCry);
		healPoints = 5; 	
	}

	public void heal(int p) {
		p *= (-1);
		if (healPoints >= p) {
			hp += p;
			healPoints -= p; 
		} 
	} 

}