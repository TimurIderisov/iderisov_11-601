package fighting;
import java.util.Random;
import java.util.Scanner;
public class Game {
	public void act() {
		
		Player p1 = new Player("Denis Popov", "Leroooooooooy Jeeenkinshshs"); 
		SmartPlayer p2 = new SmartPlayer("Kompu Docter", "ABSOLUTELY");
		int pl = 1;
		Scanner sc = new Scanner(System.in);
		Prob pr = new Prob();
		
		p1.shoutBattleCry();
		p2.shoutBattleCry();
		System.out.println("________________________");
		System.out.println(p1.getName() + " Starts." + p2.getName() + " can use healing. Just 5 healPoints. To use them write :-<INT> ");
		
		while(p1.getHp() > 0 && p2.getHp() > 0) {
			
			if(pl == 1) {
				System.out.println(p1.getName() + " attacks");
				double i = sc.nextDouble();
				if(pr.punch(i) == true) {	
					p2.dam(i);
				}	
			}
			
			else {
				System.out.println(p2.getName() + " attacks or heals");
					int j = sc.nextInt();
				if(j < 0) {
					p2.heal(j);
				}
				else {
					if(pr.punch(j) == true) {	
						p1.dam(j);
					}	
				}
			}	 
			
			if(pl == 1) {
				pl = 2;
			}
			
			else {
				pl = 1;
			}
			
			System.out.println(p1.getName() + ":" + p1.getHp());
			System.out.println(p2.getName() + ":" + p2.getHp());
		}
		
		if(p1.getHp() > p2.getHp()) {
			System.out.println(p1.getName() + " WiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiIN" );
		}
		
		else {
			System.out.println(p2.getName() + " WiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiIN");
		}
	} 
	
} 