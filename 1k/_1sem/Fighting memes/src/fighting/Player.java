package fighting;
public class Player {
	protected double hp; 
	private String name;
	private String battleCry;
	
	
	public Player(String name, String battleCry) {
		hp = 20;
		this.name = name;
		this.battleCry = battleCry;
	} 
	
	public void shoutBattleCry() { 
		System.out.println(name + ": " + battleCry); 
	} 
	
    public void dam(double d) {
		hp -= d;
    }
	
	public double getHp() {
		return hp;
	}
	
	public String getName() {
		return name;
	}
	
}