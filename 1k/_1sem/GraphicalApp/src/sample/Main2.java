package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Main2 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        Group root = new Group();
        Circle circle = new Circle(10,10,10);
        circle.setStyle("-fx-fill: #2837ff");
        root.setOnKeyPressed(event -> {

                if (event.getCode() == KeyCode.RIGHT && circle.getCenterX() + circle.getRadius() < 301 ) {
                    circle.setCenterX(circle.getCenterX() + 10);
                }
                if (event.getCode() == KeyCode.LEFT && circle.getCenterX() - circle.getRadius() > -1) {
                    circle.setCenterX(circle.getCenterX() - 10);
                }
                if (event.getCode() == KeyCode.DOWN && circle.getCenterY() + circle.getRadius() < 301) {
                    circle.setCenterY(circle.getCenterY() + 10);
                }
                if (event.getCode() == KeyCode.UP && circle.getCenterY() - circle.getRadius() > -1) {
                    circle.setCenterY(circle.getCenterY() - 10);
                }

        });

        root.getChildren().add(circle);
        primaryStage.setTitle("HW");
        primaryStage.setScene(new Scene(root, 300, 300));
        primaryStage.show();
        primaryStage.getScene().getRoot().requestFocus();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
