package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        Group root = new Group();
        TextField tf1 = new TextField();
        TextField tf2 = new TextField();
        tf2.setLayoutX(160);
        tf2.setMaxSize(100,60);
        tf1.setMaxSize(100,60);
        Button button = new Button("calculate");
        double rt = 0;
        final String[] oper = {" "};
        button.setLayoutX(32);
        button.setLayoutY(40);
        ComboBox cb = new ComboBox();
        cb.getItems().setAll(
                        "+", "-", "*", "/"
        );
        cb.setLayoutX(103);

        cb.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                oper[0] = t1;
                double ch1 = Double.parseDouble(tf1.getText());
                double ch2 = Double.parseDouble(tf2.getText());
                if(oper[0] == "+") {
                    oper[0] = ch1 + ch2 + "";
                }
                if(oper[0] == "-") {
                    oper[0] = ch1 - ch2 + "";
                }
                if(oper[0] == "*") {
                    oper[0] = ch1 * ch2 + "";
                }
                if(oper[0] == "/") {
                    oper[0] = ch1 / ch2 + "";
                }
            }
        });

        root.getChildren().addAll(button,tf1,tf2,cb);
        primaryStage.setTitle("HW");
        primaryStage.setScene(new Scene(root, 300, 80));
        primaryStage.show();
        button.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Result");
            alert.setHeaderText(oper[0]);
            alert.setContentText("Good luck");

            alert.showAndWait();
        });
    }



    public static void main(String[] args) {
        launch(args);
    }
}
