//author Timur IDerisov 11-601 Problem Set #1  Task 15
public class PS1Task15 {
	public static void main(String [] args) {
		double g = Double.parseDouble(args[0]);
		double k1 = Double.parseDouble(args[1]);
		double k2 = Math.sqrt(g * g - k1 * k1);
		double r = (k1 + k2 - g) / 2;
		System.out.println(k2);
		System.out.println(r);
	}
}