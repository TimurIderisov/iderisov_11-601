//author Timur IDerisov 11-601 Problem Set #1  Task 79
public class PS1Task79 {
	public static void main(String [] args) {
		double i = 0.1;
		double p = 1;
		while(i <= 10) {
			p = p * (1 + Math.sin(i));
			i = i + 0.1;
		}
		System.out.println(p);
	}
}	