//author Timur IDerisov 11-601 Problem Set #1  Task 13 
public class PS1Task13 {
	public static void main(String [] args) {
		double a = Double.parseDouble(args[0]);
		double t = 2 * 3.14 * Math.sqrt(a / 9.8);
		System.out.println(t);
	}
}