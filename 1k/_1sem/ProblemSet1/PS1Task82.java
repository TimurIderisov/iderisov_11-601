//author Timur IDerisov 11-601 Problem Set #1  Task 82
public class PS1Task82 {
	public static void main(String [] args) {
		double x = 101;
		double p = 1;
		for(int i = 1; i <= 64; i++) {
			if(i % 2 == 1) {
				p = p / (x - i);
			}
			else {
				p = p * (x - i);			}
		}
		System.out.println(p);
	}
}	