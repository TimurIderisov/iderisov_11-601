//author Timur IDerisov 11-601 Problem Set #1  Task 114 e, >|<
public class PS1Task114 {
	public static void main(String [] args) {
		double p = 1;
		double fac = 1;
		for(int i = 1; i <= 10; i++){
			fac = fac * i;
			p = p * (2 + 1 / fac);
		}
		System.out.print("e ...");
		System.out.println(p);
		p = 1;
		
		for(double j = 2; j <= 100; j++){
			p *= ((j + 1) / (j + 2));
		}
		System.out.print(">|< ...");
		System.out.println(p);
	}
}	