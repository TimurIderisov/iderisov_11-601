//author Timur IDerisov 11-601 Problem Set #1  Task 83
public class PS1Task83 {
	public static void main(String [] args) {
		double s = 0;
		double n = 1;
		int a = 10;
		while(s <= a){
			s = s + 1 / n;
			n++;
		}
		System.out.println(s);
		System.out.println(n);
	}
}