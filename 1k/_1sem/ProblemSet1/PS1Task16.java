//author Timur IDerisov 11-601 Problem Set #1  Task 16
public class PS1Task16 {
	public static void main(String [] args) {
		double d = Double.parseDouble(args[0]);
		double r = d / 6.28;
		double s = r * r * 3.14;
		System.out.println(s);
	}
}