//author Timur IDerisov 11-601 Problem Set #1  Task 77
public class PS1Task77 {
	public static void main(String [] args) {
		double n = Double.parseDouble(args[0]);
		
		double s1 = 1;
		double g = 0;
		double s = 0;
		double p = 1;
		int ch = 1;
		double f = 1;
		double sin = 0;
		double sinsum = 0;
		double d = Math.sqrt(2);
		double cos = 0;
		double cossum = 0;
		double e = 1;
		double k = 3 * n;
		double j = Math.sqrt(k);
		for(int i = 1; i <= n; i++){
			ch = ch * 2;
			f = f * n;
			s1 = s1 * (1 + 1 / i * i);
			
			sin = Math.sin (n);
			sinsum = 1 / (sinsum + sin);
			g = g + sinsum;
			d = Math.sqrt(d);
			
			cos = Math.cos (n);
			cossum = 1 / (cossum + cos);
			e = e * cossum / sinsum;
			

			k = Math.sqrt((k - i) + j);
		}
		System.out.println(ch);
		System.out.println(f);
		System.out.println(s1);
		System.out.println(g);
		System.out.println(d);
		System.out.println(e);
		
	}
}