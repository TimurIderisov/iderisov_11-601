//author Timur IDerisov 11-601 Problem Set #1  Task 84
public class PS1Task84 {
	public static void main(String [] args) {
		int n = 5;
		double x = 2;
		double s = 1;
		double sum = 0;
		
		for(int i = 1; i <= n; i++){
			s *= Math.sin(x);
			sum += s;
		}
		System.out.println(sum);

		sum = 0;
		
		for(int i = 1; i <= n; i++){
			sum += Math.sin(x);
			x *= x;
		}
		System.out.println(sum);
		
		sum = 0;
		s = 1;
		x = 2;
		
		for(int i = 1; i <= n; i++){
			s = Math.sin(x); 
			sum += s;
			x = Math.sin(x);
			
		}
		System.out.println(sum);
			
	
	}
}	