//author Timur IDerisov 11-601 Problem Set #1  Task 14
public class PS1Task14 {
	public static void main(String [] args) {
		double m1 = Double.parseDouble(args[0]);
		double m2 = Double.parseDouble(args[1]);
		double r = Double.parseDouble(args[2]);
		double f = 6.67 * 1e-11 *(m1 * m2) / (r * r);
		System.out.println(f);
	}
}