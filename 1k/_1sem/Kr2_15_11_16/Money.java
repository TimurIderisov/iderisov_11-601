public class Money {
	public long rub;
	public byte kop;
	
	public Money() {
		
	}
	
	public Money(long r, byte k) {
		rub = r;
		kop = k;
	}
	
	
	public String toString() {
		String result = "";
		result = rub + "," + kop;
		return result;
	}
	
	public Money add(Money m) {
		Money result = new Money();
		result.rub = rub + m.rub;
		result.kop = (byte) (kop + m.kop);
		if(result.kop >= 100) {
			result.rub ++;
			result.kop -= 100;
		}
		return result;
	}
	
	public Money sub(Money m) {
		Money result = new Money();
		result.rub = rub - m.rub;
		result.kop = (byte) (kop - m.kop);
		
		if(result.kop < 0) {
			result.rub --;
			result.kop += 100;
		}
		
		return result;
	}
	
	public double div(Money m1, Money m2) {
		double result;
		double c1 = m1.rub * 100 + m1.kop;
		double c2 = m2.rub * 100 + m2.kop;
		result = c1 / c2;
		return result;
	}
	
	public Money multNumber(int i) {
		Money result = new Money();
		result.kop = (byte) (kop * i);
		int dop = 0;
		if(result.kop >= 100) {
			dop = result.kop / 100;
		}
		result.rub = result.rub * i + dop;
		return result;
	}
	
	public Money divNumber(int i) {
		Money result = new Money();
		double c1 = (rub * 100 + kop) / i;
		c1 = c1 / 100;
		int cel = (int) c1;
		double dr = c1 - cel;
		result.rub = cel;
		result.kop = (byte) dr;
		return result;
	}
	
	public boolean equals(Money c) {
		boolean flag = false;
		if(rub == c.rub && kop == c.kop) {
			flag = true;
		}
		return flag;
	}
	
	public static void main(String [] args) {
		Money m1 = new Money(10,(byte)50);
		Money m2 = new Money(5,(byte)25);
		Money m3 = new Money();
	
		System.out.println(m1);
		System.out.println(m2);
		
		m3 = m1.add(m2);
		System.out.println(m3);
		
		m3 = m1.sub(m2);
		System.out.println(m3);
		
		/*double res = m1.div(m2);
		System.out.println(res);
		*/
		
		m3 = m1.multNumber(10);
		System.out.println(m3);
		
		m3 = m1.divNumber(5);
		System.out.println(m3);
		
		boolean bl = m1.equals(m2);
		System.out.println(bl);
	}
}