package src2;

/**
 * Created by Timur IDerisov 11-601 on 17.06.2017.
 */
public class SiteStat {
    private String name;

    public int mediumByWeek(){
        int s = 0;
        for(int a : week){
            s+=a;
        }
        return s/7;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getWeek() {
        return week;
    }

    public void setWeek(int[] week) {
        this.week = week;
    }

    public SiteStat(String name, int[] week) {
        this.name = name;

        this.week = week;
    }

    private int[] week = new int[7];
}
