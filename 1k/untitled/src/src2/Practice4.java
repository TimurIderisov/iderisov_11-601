package src2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Timur IDerisov 11-601 on 17.06.2017.
 */
public class Practice4 {

    public static ArrayList<SiteStat> readFromFile() {
        ArrayList<SiteStat> arrayList = new ArrayList();
        try {
            Scanner sc = new Scanner(new File("C:\\bbb\\untitled\\src\\src2\\in.txt"));
            while (sc.hasNext()) {
                String s = sc.nextLine();
                String s1 = s.substring(0,s.indexOf(' '));
                s = s.substring(s.indexOf(' ') + 1, s.length());
                int[] daysOfWeek = new int[7];
                String[] st = s.split(" ");
                int i = 0;
                for(String sg : st){
                    daysOfWeek[i] = Integer.parseInt(sg);
                    i++;
                }
                arrayList.add(new SiteStat(s1,daysOfWeek));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public static void writeToFile(ArrayList<SiteStat> arrayList) {
        try {
            FileWriter fw = new FileWriter("C:\\bbb\\untitled\\src\\src2\\out.txt");
            for(SiteStat sst : arrayList){
                fw.write(sst.getName() + " " + sst.mediumByWeek() + "\n");
            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ArrayList<SiteStat> ar = readFromFile();
        writeToFile(ar);
    }
}
