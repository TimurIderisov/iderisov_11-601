import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by Timur IDerisov 11-601 on 17.06.2017.
 */
public class Practice3 {

    public static ArrayList<Integer> readFromFile() {
        ArrayList<Integer> arrayList = new ArrayList();
        try {
            Scanner sc = new Scanner(new File("C:\\bbb\\untitled\\src\\in.txt"));
            while (sc.hasNext()) {
                arrayList.add(sc.nextInt());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public static void writeToFile(ArrayList<Integer> arrayList) {
        try {
            FileWriter fw = new FileWriter("C:\\bbb\\untitled\\src\\out.txt");
            fw.write(arrayList.toString());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Integer> sortingArray(ArrayList<Integer> arrayList) {
        MyComparator mc = new MyComparator();
        Collections.sort(arrayList, mc);
        return arrayList;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a = readFromFile();
        sortingArray(a);
        writeToFile(a);
    }

}
