import java.util.Comparator;

/**
 * Created by Timur IDerisov 11-601 on 17.06.2017.
 */
public class MyComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        int fives1 = 0;
        int fives2 = 0;
        while (o1 > 0) {
            if (o1 % 10 == 5)
                fives1++;
            o1 = o1 / 10;
        }
        while (o2 > 0) {
            if (o2 % 10 == 5)
                fives2++;
            o2 = o2 / 10;
        }
        return fives1 - fives2;
    }

}
