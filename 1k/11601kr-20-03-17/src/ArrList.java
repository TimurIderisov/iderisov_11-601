public class ArrList {
    private ArrElem head;

    public ArrList(int numberOfArrays){
    }

    public void addHead(double[] x) {
        ArrElem p = new ArrElem(x, head);
        head = p;
    }

    public ArrElem getHead() {
        return head;
    }

    public void setHead(ArrElem head) {
        this.head = head;
    }
}
