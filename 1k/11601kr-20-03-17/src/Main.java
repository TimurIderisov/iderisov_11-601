public class Main {
    public static void main(String[] args) {
        ArrList al = buildArrList(10);
        int o = getLen(al);
        write(al);
        System.out.print(task2(al, o) + " and ");
        System.out.println(task2(al, o) + 1 + " are our positions");
        int k = (int) (Math.random() * 10);
        System.out.println(k + " difference");
        task3(al, k);
        write(al);
    }

    //наименьшая разница между (суммой всех элементов массива + длины) двух соседних элементов, возвращает позицию первого
    public static int task2(ArrList al, int len) {
        ArrElem p = al.getHead();
        double[] a = new double[len];
        int i = 0;
        while (p != null) {
            double[] d = p.getValue();
            a[i] = 0;
            for (int j = 0; j < d.length; j++) {
                a[i] += d[j];
            }
            a[i] += d.length;
            p = p.getNext();
            i++;
        }
        int res = 0;
        double min = Math.abs(a[0] - a[1]);
        for (i = 1; i < len - 1; i++) {
            if (Math.abs(a[i] - a[i + 1]) < min) {
                res = i;
            }
        }
        return res;
    }

    // если сумма элмеетов массива < k , копирую его и вставляю до этого массива снова
    public static void task3(ArrList al, int k) {
        ArrElem p = al.getHead();
        int j = 0;
        int s = 0;
        while (p.getNext() != null) {
            double[] d = p.getNext().getValue();
            s = 0;
            for (int i = 0; i < d.length; i++) {
                s += d[i];
            }
            if (s < k) {
                addElem(al, j);
            }
            j++;
            p = p.getNext();
        }
        //случай с головой
        p = al.getHead();
        double[] d = p.getValue();
        for (int i = 0; i < d.length; i++) {
            s += d[i];
        }
        if(s < k){
            addArrElem(al,d);
        }
    }

    public static void addElem(ArrList al, int pos) {
        ArrElem p = al.getHead();
        int i = 0;
        while (p != null) {
            if (i == pos) {
                ArrElem f = new ArrElem();
                f.setValue(p.getNext().getValue());
                f.setNext(p.getNext());
                p.setNext(f);
            }
            p = p.getNext();
        }
    }

    public static ArrList buildArrList(int number) {
        ArrList al = new ArrList(number);
        for (int i = 0; i < number; i++) {
            int o = (int) (Math.random() * 20);
            double[] x = new double[o];
            for (int j = 0; j < o; j++) {
                x[j] = Math.random() * 100;
            }
            addArrElem(al, x);
        }
        return al;
    }

    public static int getLen(ArrList al) {
        ArrElem p = al.getHead();
        int k = 0;
        while (p != null) {
            k++;
            p = p.getNext();
        }
        return k;
    }

    public static void addArrElem(ArrList al, double[] d) {
        al.addHead(d);
    }

    public static void write(ArrList al) {
        ArrElem p = al.getHead();
        while (p != null) {
            double[] d = p.getValue();
            for (int l = 0; l < d.length; l++) {
                System.out.print(d[l] + " ");
            }
            System.out.println("");
            p = p.getNext();
        }
    }
}
