public class ArrElem {
    private double[] value;
    private ArrElem next;

    public ArrElem() {

    }


    public double[] getValue() {
        return value;
    }

    public ArrElem getNext() {
        return next;
    }

    public ArrElem(double[] c, ArrElem next) {
        value = c;
        this.next = next;
    }

    public void setValue(double[] value) {
        this.value = value;
    }

    public void setNext(ArrElem next) {
        this.next = next;
    }
}
