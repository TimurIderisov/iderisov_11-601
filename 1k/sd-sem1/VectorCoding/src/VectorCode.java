public class VectorCode {
    private int length;
    private Elem head;
    private int mainLength;

    public VectorCode() {

    }


    public void addHead(int x) {
        Elem p = new Elem(x, length, head);
        head = p;
        length--;
    }

    public VectorCode(int[] arr) {
        mainLength = arr.length;
        length = arr.length - 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] != 0) {
                addHead(arr[i]);
            } else {
                length--;
            }
        }
    }

    public int[] decode() {
        Elem p = head;
        int[] a = new int[mainLength];
        while (p != null) {
            a[p.getNumber()] = p.getValue();
            p = p.getNext();
        }
        return a;
    }

    public void delete(int pos) {
        Elem p = head;
        while (p != null) {
            if (p.getNext().getNumber() == pos) {
                p.setNext(p.getNext().getNext());
                break;
            }
            p = p.getNext();
        }
    }

    public void mult(int a, int c) {
        Elem p = head;
        while (p != null) {
            if (p.getValue() == a) {
                p.setValue(a * c);
            }
            p = p.getNext();
        }
    }

    public int scalarProduct(VectorCode v) {
        Elem p = head;
        Elem p1;
        int s = 0;
        while (p != null) {
            p1 = v.getHead();
            while (p1 != null) {
                if (p.getNumber() == p1.getNumber()) {
                    s += p.getValue() * p1.getValue();
                }
                p1 = p1.getNext();
            }
            p = p.getNext();
        }
        return s;
    }


    public VectorCode sum(VectorCode v) {
        int[] a = this.decode();
        int[] b = v.decode();
        VectorCode vc;
        if (a.length > b.length) {
            for (int i = 0; i < b.length; i++) {
                a[i] += b[i];
            }
            vc = new VectorCode(a);
        } else {
            for (int i = 0; i < a.length; i++) {
                b[i] += a[i];
            }
            vc = new VectorCode(b);
        }
        return vc;
    }

    public VectorCode vectorSum() {
        int[] a = this.decode();
        int[] b = new int[a.length];
        b[0] = 0;
        for (int i = 1; i < b.length; i++) {
            for (int j = a.length - i; j < a.length - 1; j++){
                b[i]+= a[j];
            }
        }
        VectorCode vc = new VectorCode(b);
        return vc;
    }


    public void insert(int k, int pos) {
        Elem p = head;
        boolean fl = true;
        while (p != null) {
            if (p.getNumber() == pos) {
                p.setValue(k);
                fl = false;
                break;
            }
            p = p.getNext();
        }
        if (fl) {
            p = head;
            while (p != null) {
                if (head.getNumber() > pos) {
                    Elem m = new Elem(k, pos, head);
                    head = m;
                    break;
                }
                if (p.getNumber() < pos && p.getNext().getNumber() > pos) {
                    Elem m = new Elem(k, pos, p.getNext());
                    p.setNext(m);
                    break;
                }
                if (pos > p.getNumber() && p.getNext() == null) {
                    Elem m = new Elem(k, pos, null);
                    p.setNext(m);
                    break;
                }
                p = p.getNext();
            }
        }
    }


    public void write() {
        Elem p = head;
        while (p != null) {
            System.out.print("(" + p.getNumber() + "," + p.getValue() + ")" + " | ");
            p = p.getNext();
            //   System.out.println("number "+ p.getNumber()+ "  ");
        }
        System.out.println("\n");
    }

    public Elem getHead() {
        return head;
    }

    public int getMainLength() {
        return mainLength;
    }
}
