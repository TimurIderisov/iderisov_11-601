public class Tester {
    public static void main(String[] args) {
        int [] arr = {11,12,13,0,15,0,17,0,13,20};

        VectorCode vc = new VectorCode(arr);
        System.out.println("Reading array...");
        vc.write();

        System.out.println("Decoding to array...");
        arr = vc.decode();
        for (int i = 0; i < arr.length; i++){
            System.out.println(arr[i]);
        }

        System.out.println("Insert element...");
        vc.insert(100,3);
        vc.write();

        System.out.println("Deleting element...");
        vc.delete(9);
        vc.write();

        int [] b = {1,1,0,0,0,0,0,0,1};
        VectorCode vc1 = new VectorCode(b);
        System.out.println("Multiplication vectors... " + vc.scalarProduct(vc1));

        System.out.println("Multiplication... ");
        vc.mult(13,10);
        vc.write();

        System.out.println("Sum of vectors...");
        vc = vc.sum(vc1);
        vc.write();

        System.out.println("VectorSum... ");
        vc = vc.vectorSum();
        vc.write();
    }
}
