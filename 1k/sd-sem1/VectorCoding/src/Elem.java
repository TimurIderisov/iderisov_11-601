public class Elem {
    private int value;
    private int number;
    private Elem next;

    public Elem(int value, int number, Elem next) {
        this.value = value;
        this.number = number;
        this.next = next;
    }


    public int getValue() {
        return value;
    }


    public Elem getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    public int getNumber() {
        return number;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
