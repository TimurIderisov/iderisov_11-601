package hw;

import java.util.Scanner;

public class HardcodedAutomata {
    public static void main(String[] args) {
        int[][] transitions = new int[4][2];
        int[] finalStates = {0, 1, 2, 3};
        transitions[0][0] = 2;
        transitions[0][1] = 1;
        transitions[1][0] = 2;
        transitions[1][1] = 3;
        transitions[2][0] = 2;
        transitions[2][1] = 3;
        transitions[3][0] = 3;
        transitions[3][1] = 3;
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        System.out.println(checkByAutomata(transitions, finalStates, s));
    }

    private static boolean checkByAutomata(int[][] transitions, int[] finalStates, String input) {
        int state = finalStates[0];

        for (int i = 0; i < input.length(); i++){
            char a = input.charAt(i);
            state = transitions [state][(int)a - 48];
        }

        if (state == 3) {
            return false;
        } else {
            return true;
        }
    }
}
