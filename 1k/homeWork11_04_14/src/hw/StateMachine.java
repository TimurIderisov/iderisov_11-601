package hw;

import java.util.Scanner;

public class StateMachine {
    public boolean checkByStatesTask1(String input) {
        char a;
        for (int i = 1; i < input.length(); i++) {
            a = input.charAt(i);
            if (a == '1') {
                return false;
            }
        }
        return true;
    }

    public StateMachine() {
    }

    public static void main(String[] args) {
        StateMachine st = new StateMachine();
        Scanner sc = new Scanner(System.in);
        System.out.println(st.checkByStatesTask1(sc.nextLine()));
    }
}
