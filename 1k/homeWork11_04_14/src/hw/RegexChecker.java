package hw;

import java.util.Scanner;

public class RegexChecker {

    public RegexChecker() {
    }

    private boolean checkByRegex(String regex, String input) {
        return (input.matches(regex));
    }

    public static void main(String[] args) {
        RegexChecker rc = new RegexChecker();
        Scanner sc = new Scanner(System.in);
        System.out.println(rc.checkByRegex("1?0*", sc.nextLine()));
    }
}
