import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class Grammar {

    private String filename = "src/grammar.txt";
    private String terminals;
    private List<String> nonterminals;
    private String startNonterminal;
    //private boolean isRightHandSide;
    private Map<String, List<RightSideElem>> productions;

    public Grammar() {
        readGrammar(filename);
    }

    private void readGrammar(String filename) {
        try {
            Scanner sc = new Scanner(new File(filename));

            terminals = sc.nextLine();
            nonterminals = Arrays.asList(sc.nextLine().split(" "));
            startNonterminal = sc.nextLine();
//          isRightHandSide = sc.nextLine().equals("R");
            productions = new HashMap<>();

            while (sc.hasNext()) {
                String[] currentLine = sc.nextLine().split(" -> ");
                //System.out.println(Arrays.toString(currentLine));
                String[] rightSides = currentLine[1].split("\\|");

                List<RightSideElem> list = Arrays.stream(rightSides)
                        .map((x) -> new RightSideElem(
                                        x.charAt(0),
                                        x.substring(1)
                                )
                        )
                        .collect(Collectors.toList());
                productions.put(currentLine[0], list);
            }
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Grammar{" +
                "terminals='" + terminals + '\'' +
                ", nonterminals=" + nonterminals +
                ", startNonterminal='" + startNonterminal + '\'' +
                ", productions=" + productions +
                '}';
    }

    public String getStartNonterminal() {
        return startNonterminal;
    }

    public Map<String, List<RightSideElem>> getProductions() {
        return productions;
    }
}