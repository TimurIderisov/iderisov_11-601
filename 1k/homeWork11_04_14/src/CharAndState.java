public class CharAndState implements Comparable<CharAndState> {
    private State state;
    private Character c;

    public int compareTo(CharAndState p){
        if(c != p.getChar()){
            return c.compareTo(p.getChar());
        } else {
            return state.getState().compareTo(p.state.getState());
        }
    }


    public CharAndState(State state, Character c) {
        this.state = state;
        this.c = c;
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof CharAndState){
            CharAndState cas = (CharAndState) o;
            return (cas.getChar() == c&& cas.getState().equals(state));
        }else{
            return false;
        }
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Character getChar() {
        return c;
    }

    public void setChar(Character c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "CharAndState{" +
                "state=" + state +
                ", c=" + c +
                '}';
    }
}


