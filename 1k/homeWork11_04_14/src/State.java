public class State implements Comparable<State>{
    @Override
    public String toString() {
        return "State{" +
                "state='" + state + '\'' +
                '}';
    }

    public boolean equals(Object o){
        if(o instanceof State){
            return state.equals((((State) o).getState()));
        } else  {
            return false;
        }
    }

    public State(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    private String state;

    @Override
    public int compareTo(State o) {
        return  state.compareTo(o.getState());
    }
}
