import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class NFA {
    private Map<CharAndState, List<State>> transitions = new TreeMap<>();
    private State finalState = new State("#KFDAF%$@NV%".hashCode() + "");
    private State startState;

    @Override
    public String toString() {
        return "NFA{" +
                "transitions=" + transitions +
                ", finalState=" + finalState +
                ", startState=" + startState +
                '}';
    }

    public NFA(Grammar g) {
        startState = new State(g.getStartNonterminal());
        Map<String, List<RightSideElem>> prods = g.getProductions();
        for (String leftSide : prods.keySet()) {
            State from = new State(leftSide);
            for (RightSideElem rse: prods.get(leftSide)) {
                Character c = rse.getTerminal();
                State to;
                if(rse.getNonterminal().equals("")) {
                    to = finalState;
                }
                else{
                    to = new State(rse.getNonterminal());
                    CharAndState cas = new CharAndState(from,c);
                    if(transitions.containsKey(cas)){
                        List<State> lst = transitions.get(cas);
                        lst.add(to);
                    } else{
                        List<State> lst = transitions.get(cas);
                        lst.add(to);
                        transitions.put(cas,lst);
                    }
                }
            }
        }
    }

}