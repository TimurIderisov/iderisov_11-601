import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class StateSet extends TreeSet<State>{
    public StateSet(List<State> states) {
        this.states = states;
    }

    public void add(State e) {}

    public List<State> getStates() {
        return states;

    }

    public void setStates(List<State> states) {
        this.states = states;
    }

    private List<State> states = new ArrayList<>();

}
