import java.io.File;
import java.io.IOException;
import java.util.*;

public class GrammarTestDrive {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<String, List<String>> grammarBank = new HashMap<>();
        loadGrammarFromFile(grammarBank);
        System.out.println(grammarBank);
        String userWord = "010";

    }

    private static boolean checkWord(String userWord, Map<String, String> grammarBank) {
        Queue<String> q = new LinkedList<>();
        q.offer("S");
        int counterOfLenght = 1;
        while (counterOfLenght > 0) {
            String currentElem = q.poll();
            if (userWord.equals(currentElem)) {
                System.out.println("GOOD");
            } else {
                for (String key: grammarBank.keySet()) {
                    //TODO currentElem or userWord
                    if (currentElem.contains(key)) {

                    }
                }
            }
        }
        return false;
    }

    private static void loadGrammarFromFile(Map<String, List<String>> map) {
        try {
            Scanner sc = new Scanner(new File("src/readme.txt"));
            while (sc.hasNext()) {
                String currentRawLine = sc.nextLine();
                String[] currentLine = currentRawLine.split(" -> ");
                String[] values = currentLine[1].split("\\|");
                map.put(currentLine[0], Arrays.asList(values));
            }
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}