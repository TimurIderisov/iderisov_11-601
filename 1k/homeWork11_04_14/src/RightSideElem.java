public class RightSideElem {
    private Character terminal;
    private String nonterminal;

    public RightSideElem(Character terminal) {
        this.terminal = terminal;
    }

    public RightSideElem(Character terminal, String nonterminal) {
        this.terminal = terminal;
        this.nonterminal = nonterminal;
    }

    public Character getTerminal() {
        return terminal;
    }

    public void setTerminal(Character terminal) {
        this.terminal = terminal;
    }

    public String getNonterminal() {
        return nonterminal;
    }

    public void setNonterminal(String nonterminal) {
        this.nonterminal = nonterminal;
    }

    @Override
    public String toString() {
        return "T:" + terminal + ", NT:" + nonterminal;
    }
}