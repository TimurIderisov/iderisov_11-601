S -> A1|B0
A -> A0|1
B -> B1|B0|0

RegexChecker
    boolean checkByRegex(String input, String regex)

HardcodedAutomata
    boolean checkByAutomata(int[][] transition, int[] finalStates, String input) {}

StateMachine
    checkByStateMachineTask1(String input) {}
    checkByStateMachineTask2(String input) {}

RegularGrammar
    RegularGrammar(String filename)

DNAutomata
    DNAutomata(RegularGrammar grammar)

DFAutomata
    DAautomata(DNAutomata)
    check(String input)


терминальные символы
нетерминальные символы
стартовый нетерминал
переходы