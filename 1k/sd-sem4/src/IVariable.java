/**
 * Created by Timur IDerisov 11-601 on 17.05.2017.
 */
public class IVariable {

    private String id;
    private int val;

    public int getVal() {
        return val;
    }

    public void setVal(int v) {
        val = v;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return '{' + "id='" + id + '\'' +
                ", val=" + val +
                "}    ";
    }

    public IVariable(String id) {
        this.id = id;
    }

    public IVariable(String id, int val) {
        this.id = id;
        this.val = val;
    }
}
