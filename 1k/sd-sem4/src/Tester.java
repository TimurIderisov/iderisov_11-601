import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Timur IDerisov 11-601 on 28.05.2017.
 */
public class Tester {
    public static void main(String[] args) throws IOException {
        try {
            testing();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("СИНТАКСИЧЕСКЯ ПРОВЕРКА ПРОВАЛЕНА");
        }

    }

    public static void testing() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("src\\program"));
        String s = reader.readLine();
        LexicalAnalyzer la = new LexicalAnalyzer();
        boolean m = Syntax.analysis(s);
        if (la.analyze(s) == true) {
            System.out.println("УСПЕШНАЯ ЛЕКСИЧЕСКАЯ ПРОВЕРКА");
            if (m) {
                System.out.println("УСПЕШНАЯ СИНТАКСИЧЕСКЯ ПРОВЕРКА");
                System.out.println("УСПЕШНАЯ СЕМАНТИЧЕСКАЯ ПРОВЕРКА");
                ProgramInter p = new ProgramInter();
                p.readProg();
            } else {
                System.out.println("СИНТАКСИЧЕСКЯ ПРОВЕРКА ПРОВАЛЕНА");
            }
        } else {
            System.out.println("ЛЕКСИЧЕСКАЯ ПРОВЕРКА ПРОВАЛЕНА");
        }
    }
}
