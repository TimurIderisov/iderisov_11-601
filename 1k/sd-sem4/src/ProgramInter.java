import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Timur IDerisov 11-601 on 17.05.2017.
 */
public class ProgramInter {
    private static ArrayList<IVariable> iVariables = new ArrayList<>();

    public ProgramInter() throws FileNotFoundException {
    }


    private static boolean isPluce(String s) {
        char c = s.charAt(0);
        if (c == '+') {
            return true;
        }
        return false;
    }

    public static void readProg() throws IOException {
        FileReader fr = new FileReader("src\\program");
        BufferedReader bf = new BufferedReader(fr);
        String s = bf.readLine();
        String[] operators = s.split(";");
        for (String operator : operators) {
            String[] a = operator.split(":=");
            IVariable iv = new IVariable(a[0]);
            operatorTranslator(a[1], iv);
            iVariables.add(iv);
        }
        System.out.println(iVariables);
    }

    private static void operatorTranslator(String s, IVariable iVariable) {
        int i = 0;
        int numberOfVars = 0;
        Pattern ident = Pattern.compile("([a-z]([a-zA-Z]|[0-9])*)|((-|\\+)[0-9])");
        Matcher id = ident.matcher(s);
        while (id.find()) numberOfVars++;
        if (numberOfVars == 1) {
            if (letterFinder(s.charAt(0))) {
                String identi = s.substring(0, s.length());
                iVariable.setVal(iVariables.stream().filter(o -> o.getId().equals(identi)).findFirst().get().getVal());
            } else {
                if (isPluce(s)) iVariable.setVal(Integer.parseInt(s.substring(1, s.length())));
                else iVariable.setVal(-1 * Integer.parseInt(s.substring(1, s.length())));
            }
        } else {
            int[] vars = new int[2];
            int j = 0;
            int signPos = 0;
            int flag = 0;
            if (s.charAt(0) == '+') {
                flag = 1;
                s = s.substring(1, s.length());
            }
            if (s.charAt(0) == '-') {
                flag = 2;
                s = s.substring(1, s.length());
            }

            String[] ss = s.split("((\\+|-|\\*|/)(-|\\+))|(\\+|-|\\*|/)");
            for (String str : ss) {
                if (letterFinder(str.charAt(0))) {
                    String identi = str.substring(0, str.length());
                    vars[j] = (iVariables.stream().filter(o -> o.getId().equals(identi)).findFirst().get().getVal());
                } else {
                    vars[j] = (Integer.parseInt(str.substring(0, str.length())));
                }
                signPos = s.length() - str.length() - 1;
                j++;
            }
            if (s.charAt(signPos - 1) == '+' || s.charAt(signPos - 1) == '-' || s.charAt(signPos - 1) == '*' || s.charAt(signPos - 1) == '/') {
                if (s.charAt(signPos) == '-') {
                    vars[1] *= -1;
                }
                signPos--;
            }
            if (flag == 2) {
                vars[0] *= -1;
            }


            if (s.charAt(signPos) == '+') {
                iVariable.setVal(vars[0] + vars[1]);
            }
            if (s.charAt(signPos) == '-') {
                iVariable.setVal(vars[0] - vars[1]);
            }
            if (s.charAt(signPos) == '*') {
                iVariable.setVal(vars[0] * vars[1]);
            }
            if (s.charAt(signPos) == '/') {
                iVariable.setVal(vars[0] / vars[1]);
            }
        }
    }

    private static boolean digitFinder(Character c) {
        int a = (int) c;
        if (a >= 48 && a <= 57) {
            return true;
        }
        return false;
    }

    private static boolean letterFinder(Character c) {
        int a = (int) c;
        if ((a >= 65 && a <= 90) || (a >= 97 && a <= 122)) {
            return true;
        }
        return false;
    }

}
