/**
 * Created by Андрей on 25.05.2017.
 */
public class LexicalAnalyzer {

    public boolean analyze(String s) {
        s = s.replaceAll(" ", "");
        String[] list = s.split(":=");
        char[] her;
        int n = list.length;
        if (!isId(list[0])) {
            return false;
        }
        for (int i = 1; i < n; i++) {
            if (!hearth(list[i])) {
                return false;
            }
        }
        return true;
    }

    private boolean isDigit(char c) {
        if (c < 48 || c > 57) {
            return false;
        }
        return true;
    }

    private boolean isLetter(char c) {
        if ((c > 64 && c < 91) || (c > 96 && c < 123)) {
            return true;
        }
        return false;
    }

    private boolean isId(String s) {
        if (s.length() > 2) {
            return false;
        }
        char[] her = s.toCharArray();
        if (!isLetter(her[0])) {
            return false;
        }
        if (her.length == 2) {
            if (!isLetter(her[1]) && !isDigit(her[1])) {
                return false;
            }
        }
        return true;
    }

    private boolean isNum(String s) {
        if (s.length() > 5) {
            return false;
        }
        char[] her = s.toCharArray();
        for (int i = 0; i < her.length; i++) {
            if (!isDigit(her[i])) {
                return false;
            }
        }
        return true;
    }

    private boolean isOper(char c) {
        char[] her = "+-−*/".toCharArray();
        for (int i = 0; i < her.length; i++) {
            if (c == her[i]) {
                return true;
            }
        }
        return false;
    }

    private boolean isOper(String s) {
        if (s.length() > 2) {
            return false;
        }
        char[] her = s.toCharArray();
        if (s.length() == 1) {
            return true;
        }
        if (her[1] == '*' || her[1] == '/') {
            return false;
        }
        return true;
    }

    private int bow(char c) {
        if (isOper(c)) {
            return 3;
        }
        if (isDigit(c)) {
            return 2;
        }
        if (isLetter(c)) {
            return 1;
        }
        return 4;
    }

    private boolean hearth(String s) {
        String[] fa = s.split(";");
        if (fa.length > 2) {
            return false;
        }
        char[] he = fa[0].toCharArray();
        for (int i = 0; i < he.length; i++) {
            s = "";
            if (isLetter(he[i]) && i + 1 != he.length) {
                if (bow(he[i + 1]) < 3) {
                    s += he[i];
                    s += he[i + 1];
                    if (i + 2 < he.length) {
                        if (!isOper(he[i + 2])) {
                            return false;
                        }
                    }
                    i++;
                } else {
                    s += he[i];
                }
                if (!isId(s)) {
                    return false;
                }
                continue;
            }
            if (isOper(he[i])) {
                if (i + 1 >= he.length) {
                    return false;
                }
                if (bow(he[i + 1]) > 3) {
                    return false;
                }

                while (isOper(he[i])) {
                    s += he[i];
                    i++;
                }
                if (!isOper(s)) {
                    return false;
                }
                if (i == 2 && isId(he[i] + "") && s.length() != 1) {
                    return false;
                }
                if (s.length() == 1 && i != 1) {
                    if (!isId(he[i] + "")) {
                        return false;
                    }
                }
                i--;
                continue;
            }
            if (isDigit(he[i])) {
                if (i == 0) {
                    return false;
                }
                if (!isOper(he[i - 1])) {
                    return false;
                }
                while (i + 1 < he.length) {
                    if (!isDigit(he[i + 1])) {
                        break;
                    }
                    i++;
                }
                continue;
            }
        }
        if (fa.length == 1) {
            return true;
        }
        if (!isId(fa[1])) {
            return false;
        }

        return true;
    }

}
