import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by lucky.mz on 27.05.2017.
 */
public final class Syntax {
    private static String oper = "[\\*+-/]";
    private static String appropriator = ":=";
    private static String variable = "[a-z](\\w)*";
    private static String number = "[+-](\\d)+(.\\d+)?";
    private static String space = "\\_*";
    private static String closer = ";";
    private static String expression1 = "(" + variable + space + appropriator
            + space + number + space + "(" + oper + number + ")?"
            + space + closer + ")";
    private static String expression2 = "(" + variable + space + appropriator
            + space + variable + space + "(" + oper + number + ")?"
            + space + closer + ")";
    private static String expression3 = "(" + variable + space + appropriator
            + space + number + space + "(" + oper + variable + ")?"
            + space + closer + ")";
    private static String expression4 = "(" + variable + space + appropriator
            + space + variable + space + "(" + oper + variable + ")?"
            + space + closer + ")";
    private static ArrayList<String> variables = new ArrayList<>();

    public static boolean analysis(String s) {
        String[] sa = s.split(";");
        String[] sd;
        String pat;
        boolean fl = true;
        for (int i = 0; i < sa.length && fl; i++) {
            sa[i] += ";";
            sd = sa[i].split(" ");
            pat = "";
            for (int j = 0; j < sd.length; j++) {
                pat += sd[j];
            }
            fl = check(pat);
        }
        if (fl) return true;
        else return false;
    }

    private static boolean check(String s) {
        boolean op = checkOper(s);
        Pattern[] pat = new Pattern[4];
        pat[0] = Pattern.compile("^" + expression1 + "$");
        pat[1] = Pattern.compile("^" + expression2 + "$");
        pat[2] = Pattern.compile("^" + expression3 + "$");
        pat[3] = Pattern.compile("^" + expression4 + "$");
        boolean fl = false;
        int cursor = 0;
        for (int i = 0; i < pat.length && !fl; i++) {
            fl = pat[i].matcher(s).find();
            cursor = i;
        }
        boolean var;
        var = checkVar(s, cursor);

        if (fl && var && op) return true;
        else return false;
    }

    private static boolean checkOper(String s) {
        int k = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '+' || s.charAt(i) == '-'
                    || s.charAt(i) == '*' || s.charAt(i) == '/') {
                k++;
            }
        }
        if (k > 3) return false;
        else return true;
    }

    private static boolean checkVar(String s, int cursor) {
        String[] xy = s.split(";");
        s = xy[0];
        String[] sa = s.split(":=");
        variables.add(sa[0]);
        if (cursor > 0) {
            if (cursor == 3) {
                int k = 0;
                String[] sp = sa[1].split(oper);
                k = 0;
                for (int i = 0; i < variables.size(); i++) {
                    for (int j = 0; j < sp.length; j++) {
                        if (sp[j].equals(variables.get(i))) {
                            k++;
                            sp[j] = "";
                        }
                    }
                    if (k >= 2) return true;
                }
                if (k >= 2) return true;
                else return false;
            } else {
                int k = 0;
                String[] sp = sa[1].split(oper);
                for (int i = 0; i < variables.size(); i++) {
                    k = 0;
                    for (int j = 0; j < sp.length; j++) {
                        if (sp[j].equals(variables.get(i))) k++;
                    }
                    if (k >= 1) return true;
                }
                if (k >= 1) return true;
                else return false;
            }
        } else return true;
    }
}

