public interface TreeVisitor {
    public int visit(Tree node);
};
