import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Random;

public class ArrayGenerator {
    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 50; i++) {
            recordToFile(buildArray(100 + 198 * i));
        }
    }

    public static int[] buildArray(int l) {
        Random r = new Random();
        int a[] = new int[l];
        for (int i = 0; i < l; i++) {
            a[i] = r.nextInt() / 10000;
        }
        return a;
    }

    public static void recordToFile(int[] a) throws IOException {
        String array = Arrays.toString(a);
        array = array.substring(1, array.length()-1);
        Files.write(Paths.get("C:/bbb/sd-sem2/src/Arrays.txt"), (array + "\n").getBytes(), StandardOpenOption.APPEND);
    }

}
