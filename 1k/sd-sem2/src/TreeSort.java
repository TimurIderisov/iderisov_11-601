import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TreeSort {

    public static int[] arrBuilder(String[] str, int size) {
        int[] a = new int[size];
        for (int j = 0; j < size; j++) {
            a[j] = Integer.parseInt(str[j]);
        }
        return a;
    }

    public static ArrayList<Integer> arrLsitBuilder(String[] str, int size) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        for (int j = 0; j < size; j++) {
            a.add(Integer.parseInt(str[j]));
        }
        return a;
    }

    public static LinkedList<Integer> linkedListBuilder(String[] str, int size) {
        LinkedList<Integer> a = new LinkedList<Integer>();
        for (int j = 0; j < size; j++) {
            a.add(Integer.parseInt(str[j]));
        }
        return a;
    }

    public static void sortingProcess(int[] a, LinkedList<Integer> ll, ArrayList<Integer> al, int size) throws FileNotFoundException {
        Tree myTree;

        //array[]
        long nanoTime1 = System.nanoTime();
        long time1 = System.currentTimeMillis();
        myTree = new Tree(a[0]);
        for (int l = 1; l < size; l++) {
            myTree.insert(new Tree(a[l]));
        }
        myTree.traverse(new GetKey());
        long nanoTime2 = System.nanoTime();
        long time2 = System.currentTimeMillis();

        System.out.println("int[]");
        System.out.println(time2 - time1 + "millis");
        myTree.printCounter();

        System.out.println("");

        //ArrayList
        nanoTime1 = System.nanoTime();
        time1 = System.currentTimeMillis();
        myTree = new Tree(al.get(0));
        for (int l = 1; l < size; l++) {
            myTree.insert(new Tree(al.get(l)));
        }
        myTree.traverse(new GetKey());
        nanoTime2 = System.nanoTime();
        time2 = System.currentTimeMillis();

        System.out.println("ArrayList");
        System.out.println(time2 - time1 + " millis");
        System.out.println("");


        //LinkedList
        nanoTime1 = System.nanoTime();
        time1 = System.currentTimeMillis();
        myTree = new Tree(ll.get(0));
        for (int l = 1; l < size; l++) {
            myTree.insert(new Tree(ll.get(l)));
        }
        myTree.traverse(new GetKey());
        nanoTime2 = System.nanoTime();
        time2 = System.currentTimeMillis();

        System.out.println("LinkedList");
        System.out.println(time2 - time1 + " millis");
        System.out.println("");

    }

    public static void main(String args[]) throws IOException {
        int size = 100;
        String[] s = readFromFile();
        int[] a;
        ArrayList<Integer> b;
        LinkedList<Integer> c;
        for (int i = 0; i < 50; i++) {
            String[] str = s[i].split(", ");

            a = arrBuilder(str, size);
            b = arrLsitBuilder(str, size);
            c = linkedListBuilder(str, size);

            sortingProcess(a, c, b, size);
            size += 198;
        }
    }

    public static String[] readFromFile() throws IOException {
        BufferedReader bf = new BufferedReader(new FileReader("C:/bbb/sd-sem2/src/Arrays.txt"));
        String line;
        List<String> lines = new ArrayList<String>();
        while ((line = bf.readLine()) != null) {
            lines.add(line);
        }
        String[] linesAsArray = lines.toArray(new String[lines.size()]);
        return linesAsArray;
    }
}