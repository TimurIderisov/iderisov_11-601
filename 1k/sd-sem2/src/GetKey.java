public class GetKey implements TreeVisitor {
    public int visit(Tree node) {
        int a = node.getCounter();
        a++;
        node.setCounter(a);
        return node.key;
    }
}
