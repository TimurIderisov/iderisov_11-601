public class Tree {
    public Tree left;
    public Tree right;
    public int key;
    private static int counter = 0;

    public Tree(int k) {
        key = k;
    }

    public void insert(Tree aTree) {
        counter++;
        if (aTree.key < key)
            if (left != null)
                left.insert(aTree);
            else
                left = aTree;

        else if (right != null)
            right.insert(aTree);
        else right = aTree;
    }

    public void traverse(TreeVisitor visitor) {
        if (left != null)
            left.traverse(visitor);

        visitor.visit(this);

        if (right != null)
            right.traverse(visitor);
    }

    public void printCounter() {
        System.out.println(counter + " iterations");
    }

    public void setCounter(int i) {
        counter = i;
    }


    public int getCounter() {
        return counter;
    }
}