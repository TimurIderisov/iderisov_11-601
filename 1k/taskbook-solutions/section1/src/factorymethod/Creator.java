package factorymethod;

/**
 * Created by Timur IDerisov 11-601 on 06.05.2017.
 */
public interface Creator<T> {
    public T factoryMethod();
}
