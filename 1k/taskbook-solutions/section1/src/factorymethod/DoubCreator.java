package factorymethod;

/**
 * Created by Timur IDerisov 11-601 on 06.05.2017.
 */
public class DoubCreator implements Creator {
    @Override
    public Object factoryMethod() {
        RandDoub rd = new RandDoub();
        return rd.generate();
    }
}
