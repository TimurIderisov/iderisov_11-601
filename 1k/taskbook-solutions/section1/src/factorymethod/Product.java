package factorymethod;

import java.util.Random;

/**
 * Created by Timur IDerisov 11-601 on 06.05.2017.
 */
public interface Product<T> {
    public T generate();
}
