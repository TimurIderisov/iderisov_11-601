package factorymethod;

/**
 * Created by Timur IDerisov 11-601 on 06.05.2017. TASK000
 */
public class Main {
    public static void main(String[] args) {
        IntCreator i = new IntCreator();
        DoubCreator d = new DoubCreator();
        System.out.println(i.factoryMethod() + "," + d.factoryMethod());
    }
}
