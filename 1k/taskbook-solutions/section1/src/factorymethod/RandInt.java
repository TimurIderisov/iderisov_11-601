package factorymethod;

import java.util.Random;

/**
 * Created by Timur IDerisov 11-601 on 06.05.2017.
 */
public class RandInt implements Product{
    @Override
    public Object generate() {
        Random r = new Random();
        return (r.nextInt());
    }
}
