package factorymethod;

/**
 * Created by Timur IDerisov 11-601 on 06.05.2017.
 */
public class IntCreator implements Creator {
    @Override
    public Object factoryMethod() {
        RandInt ri = new RandInt();
        return ri.generate();
    }
}
