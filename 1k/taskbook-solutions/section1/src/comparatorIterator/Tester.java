package comparatorIterator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class Tester {
    public Tester() throws IOException {
    }

    public static void main(String[] args) throws IOException {
        specSort();
        ArrayList<Vector2D> a = new ArrayList<>();
        a.add(0, new Vector2D(12, 12));
        a.add(1, new Vector2D(143, 143));
        a.add(2, new Vector2D(2, 4));
        a.add(2, new Vector2D(2, 77));
        sortStrByChar();
        //Task046 and 047
        Collections.sort(a, new VectorComparator());
        Collections.sort(a);
        System.out.println(a);
    }

    //Task045
    public static void specSort() throws IOException {
        ArrayList<Integer> ar = new ArrayList<>();
        Scanner sc = new Scanner(new File("src\\comparatorIterator\\in.txt"));
        while (sc.hasNext()) {
            ar.add(sc.nextInt());
        }
        Comp c = new Comp();
        ArrayList ar2 = new ArrayList();
        int ff = ar.size();
        while (ar2.size() != ff) {
            Iterator i = ar.iterator();
            int min = (int) i.next();
            while (i.hasNext()) {
                int t = (int) i.next();
                if (c.compare(min, (Integer) t) > 0) min = t;
            }
            ar2.add(min);
            i = ar.iterator();
            while (i.hasNext()) {
                int t = (int) i.next();
                if (t == min) i.remove();
            }
        }
        FileWriter fr = new FileWriter("src\\comparatorIterator\\out.txt");
        fr.write(ar2.toString());
        fr.close();
    }

    //Task048
    public static void sortStrByChar() throws IOException {
        Scanner sc = new Scanner(new File("src\\comparatorIterator\\in1.txt"));
        int l = sc.nextInt();
        ArrayList arr = new ArrayList();
        while (sc.hasNext()) {
            String s = sc.nextLine();
            if (s.length() >= l) arr.add(s);
        }
        FileWriter fr = new FileWriter("src\\comparatorIterator\\out2.txt");
        for (int i = 0; i < l; i++) {
            Collections.sort(arr, new StrComp(l - 1));
            fr.write(arr.toString());
        }
        fr.close();
    }

}
