package comparatorIterator;

import java.util.Comparator;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class Comp implements Comparator<Integer>{

    @Override
    public int compare(Integer o1, Integer o2) {
        int s1=0;
        while (o1>0){
            o1/=10;
            s1++;
        }
        int s2 =0;
        while (o2>0){
            o2/=10;
            s2++;
        }
        if(s1>s2) return 1;
        else return -1;
    }


}
