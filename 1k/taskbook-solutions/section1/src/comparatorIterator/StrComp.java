package comparatorIterator;

import java.util.Comparator;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class StrComp implements Comparator<String> {
    private int k;

    public StrComp(int i) {
        k = i  ;
    }

    @Override
    public int compare(String o1, String o2) {
        if(o1.charAt(k)> o2.charAt(k)) return 1;
        else if(o1.charAt(k)< o2.charAt(k)) return -1;
        else return 0;
    }
}
