package comparatorIterator;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class Vector2D implements Comparable {
    private double x, y;
    public static double l;

    //Task046
    @Override
    public int compareTo(Object o) {
        Vector2D v = (Vector2D) o;
        if(getLength()> v.getLength()) return -1;
        else if(getLength()== v.getLength()) return 1;
        else return 0;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D() {
    }

    public Vector2D add(Vector2D v) {
        Vector2D result = new Vector2D(x + v.getX(), y + v.getY());
        return result;
    }

    public void add2(Vector2D v2) {
        this.x = x + v2.getX();
        this.y = y + v2.getY();
    }

    public Vector2D sub(Vector2D v) {
        Vector2D result = new Vector2D(x - v.getX(), y - v.getY());
        return result;
    }

    public void sub2(Vector2D v2) {
        this.x = x - v2.getX();
        this.y = y - v2.getY();
    }

    public Vector2D mult(double a) {
        Vector2D result = new Vector2D(x * a, y * a);
        return result;
    }

    public void mult2(double a) {
        this.x = x * a;
        this.y = y * a;
    }

    public String toString() {
        return "<" + x + ", " + y + ">";
    }


    public boolean equals(Vector2D v2) {
        boolean flag = false;
        if (Math.sqrt(x * x + y * y) == v2.getLength()) {
            flag = true;
        }
        return flag;
    }


    public double scalarProduct(Vector2D v) {
        double result = x * v.getX() + y * v.getY();
        return result;
    }

    public double getLength() {
        l = Math.sqrt(x * x + y * y);
        return l;
    }

    public static double cosOfAngel(Vector2D v, Vector2D c) {
        l = (v.getX() * c.getX() + v.getY() * c.getY()) / (v.getLength() * c.getLength());
        return l;
    }

}