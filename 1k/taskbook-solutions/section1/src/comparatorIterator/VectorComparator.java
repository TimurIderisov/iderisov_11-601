package comparatorIterator;

import java.util.Comparator;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class VectorComparator implements Comparator<Vector2D> {
    @Override
    public int compare(Vector2D o1, Vector2D o2) {
        if(o1.getX()>o2.getX()) return 1;
        else if(o1.getX() < o2.getX()) return  -1;
        else {
            if(o1.getY() > o2.getY()) return 1;
            if(o1.getY() < o2.getY()) return -1;
            else return 0;
        }
    }
}
