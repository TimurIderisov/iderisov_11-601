package collections;

/**
 * Created by Timur IDerisov 11-601 on 30.05.2017.
 */
public class Elem<T> {
    public Elem(T value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public T getValue() {
        return value;

    }

    public void setValue(T value) {
        this.value = value;
    }

    public Elem getNext() {
        return next;
    }

    public Elem() {
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    private T value;
    private Elem next;
}
