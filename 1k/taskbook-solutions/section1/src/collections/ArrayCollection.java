package collections;

import java.util.Collection;
import java.util.Iterator;

/** TASK 039 AND TASK 040
 * Created by Timur IDerisov 11-601 on 30.05.2017.
 */
public class ArrayCollection<T> implements java.util.Collection<T> {
    private final int CAPACITY = 32000;
    private int size = 0;
    private T[] arr = (T[]) new Object[CAPACITY];


    @Override
    public int size() {
        return arr.length;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) return true;
        return false;
    }

    @Override
    public boolean contains(Object o) {
        for (Object t : arr) {
            if (t == o) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        arr[size()] = (T) o;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (arr[i] == o) {
                for (int j = i; j < size - 1; j++) {
                    arr[j] = arr[j + 1];
                }
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        try {
            for (Object t : c) {
                add(t);
                return true;
            }
        } catch (Exception e) {
            System.out.println("oops");
        }
        return false;
    }

    @Override
    public void clear() {
        size = 0;
    }

    @Override
    public boolean retainAll(Collection c) {
        size =0;
        for(Object t : c){
            for(T k: arr){
                if(t == k){
                    add(t);
                }
            }
        }
        if(size>0) return true;
        else return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean b = false;
        for (Object t : c) {
            b = remove(t);
        }
        return b;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}
