package collections;

import java.util.Collection;
import java.util.Iterator;

/**
 * TASK 041 AND TASK 042
 * Created by Timur IDerisov 11-601 on 30.05.2017.
 */
public class MyLinkedCollection<T> implements java.util.Collection<T> {
    Elem<T> head = new Elem<T>();


    @Override
    public int size() {
        int i = 0;
        Elem t = head;
        while (t != null) {
            i++;
            t = t.getNext();
        }
        return i;
    }

    @Override
    public boolean isEmpty() {
        return (0 == size());
    }

    @Override
    public boolean contains(Object o) {
        Elem p = head;
        while (p!=null){
            if(p.getValue() == o) return true;
            p = p.getNext();
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        Elem p = head;
        while (p.getNext() != null) {
            p = p.getNext();
        }
        p.setNext(new Elem(t, null));
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Elem p = head;
        while (p!=null){
            if(p.getNext().getValue() == o){
                p.setNext(p.getNext().getNext());
                return true;
            }
            p = p.getNext();
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for(Object t: c){
            if(!contains(t)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        try {

            for (Object o : c) {
                add((T) o);
            }
            return true;
        } catch (Exception e){}
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for(Object t : c){
            boolean b = remove(t);
            if(!b) return false;
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Elem p = head;
        while (p != null) {
            boolean b = false;
            for (Object t : c) {
                if (p.getNext().getValue() == t) {
                    b = true;
                    break;
                }
            }
            if (!b) {
                p.setNext(p.getNext().getNext());
                p = p.getNext();
            }
        }
        return true;
    }

    public void clear() {
        head = null;
    }
}
