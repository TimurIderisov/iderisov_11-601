package collections;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * TASK 044
 * Created by Timur IDerisov 11-601 on 30.05.2017.
 */
public class MyLinkedList<T> extends MyLinkedCollection implements java.util.List {
    private Elem head;

    @Override
    public boolean addAll(int index, Collection c) {
        try {
            Elem p = head;
            while (index > 0) {
                index--;
                p = p.getNext();
            }
            Elem m = p.getNext();
            for (Object t : c) {
                p.setNext(new Elem(t, null));
                p = p.getNext();
            }
            p.setNext(m);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public Object get(int index) {
        Elem p = head;
        while (index > 0) {
            index--;
            p = p.getNext();
        }
        return p;
    }

    @Override
    public Object set(int index, Object element) {
        Elem p = head;
        while (index > 0) {
            index--;
            p = p.getNext();
        }
        Elem m = p.getNext().getNext();
        p.setNext(new Elem(element, null));
        return true;
    }

    @Override
    public void add(int index, Object element) {
        Elem p = head;
        while (index > 0) {
            index--;
            p = p.getNext();
        }
        Elem m = p.getNext().getNext();
        p.setNext(new Elem(element, null));
    }

    @Override
    public Object remove(int index) {
        Elem p = head;
        while (index > 0) {
            index--;
            p = p.getNext();
        }
        Elem m = p;
        p.setNext(p.getNext().getNext());
        return m;
    }

    @Override
    public int indexOf(Object o) {
        Elem p = head;
        int i = 0;
        while (p.getValue() != o) {
            p = p.getNext();
            i++;
        }
        return i;
    }

    @Override
    public int lastIndexOf(Object o) {
        Elem p = head;
        int i = 0;
        int j = -1;
        while (p != null) {
            if (p.getValue() == o) j = i;
            p = p.getNext();
            i++;
        }
        return j;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        Elem p = head;
        toIndex -= fromIndex;
        while (fromIndex > 0) {
            fromIndex--;
            p = p.getNext();
        }
        head = p;
        while (toIndex > 0) {
            toIndex--;
            p = p.getNext();
        }
        p.setNext(null);
        return this;
    }
}
