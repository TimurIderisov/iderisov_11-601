package collections;

import stackQueue.MyLinkedStack;

import java.util.Collection;
import java.util.Iterator;

/** TASK038
 * Created by Timur IDerisov 11-601 on 30.05.2017.
 */
public class MyLinkedQueue<T> implements java.util.Queue<T> {
    MyLinkedStack s1 = new MyLinkedStack<T>();
    MyLinkedStack s2 = new MyLinkedStack<T>();
    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        s1.push(t);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(T t) {
        s1.push(t);
    return true;
    }

    @Override
    public T remove() {
        if(s1.isEmpty()) throw new IllegalArgumentException("is empty");
        return poll();
    }

    @Override
    public T poll() {
        T t = null;
        while (!s1.isEmpty()) {
            s2.push(s1.pop());
            t = (T) s2.pop();
        }
        while (!s2.isEmpty()) {
            s1.push(s2.pop());
        }
        return t;
    }

    @Override
    public T element() {
        if(s1.isEmpty()) throw new IllegalArgumentException("is empty");
        return peek();
    }

    @Override
    public T peek() {
        T t = null;
        while (!s1.isEmpty()) {
            s2.push(s1.pop());
            t = (T) s2.peek();
        }
        while (!s2.isEmpty()) {
            s1.push(s2.pop());
        }
        return t;
    }
}
