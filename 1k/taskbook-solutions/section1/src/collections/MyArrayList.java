package collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Timur IDerisov 11-601 on 30.05.2017.
 */
public class MyArrayList<T> extends ArrayCollection implements java.util.List<T> {
    private final int CAPACITY = 32000;
    private T[] arr = (T[]) new Object[CAPACITY];
    private int size = 0;

    public MyArrayList(T[] arr){
        for(int i = 0; i < arr.length;i++){
            this.arr[i] = arr[i];
        }
    }

    @Override
    public boolean addAll(int index, Collection c) {
        try {
            int i = size;
            for (Object o : c) {
                arr[i] = (T) o;
                i++;
                size--;
            }
            return true;
        }catch (Exception e) {}
        return false;
    }


    @Override
    public T get(int index) {
        return arr[index];
    }

    @Override
    public Object set(int index, Object element) {
        T t = arr[index];
        arr[index] = (T) element;
        return t;
    }

    @Override
    public void add(int index, Object element) {
        arr[index] = (T) element;
    }

    @Override
    public T remove(int index) {
        T t = arr[index];
        for(int i = index; i < size()-1; i++){
            arr[i] = arr[i+1];
        }
        size--;
        return t;
    }

    @Override
    public int indexOf(Object o) {
        for(int i = 0; i < size();i++){
            if(arr[i] == o){
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int k = -1;
        for(int i = 0; i < size();i++){
            if(arr[i] == o){
                k= i;
            }
        }
        return k;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        T[] arr1 = (T[]) new Object[CAPACITY];
        for(int i = fromIndex; i < toIndex;i++){
            arr1[i-fromIndex] = arr[i];
        }
        MyArrayList a = new MyArrayList(arr1);
        return a;
    }
}
