package trees;

import java.util.*;

public class BinaryIntTree {

    private Node<Integer> root;
    private int mult = 1;
    private int sum = 0;
    private int max= 0;

    private static void makeTreeOnNode(Node<Integer> node, int n) {
        int nLeft = n / 2;
        int nRight = n - 1 - nLeft;
        node.setValue(n);
        if (nLeft > 0) {
            node.setLeft(new Node<Integer>());
            makeTreeOnNode(node.getLeft(), nLeft);
        }
        if (nRight > 0) {
            node.setRight(new Node<Integer>());
            makeTreeOnNode(node.getRight(), nRight);
        }
    }

    public BinaryIntTree(int n) {
        root = new Node<Integer>();
        makeTreeOnNode(root, n);
    }

    public void printTree() {
        printNode(root, 0);
    }


    private static int counter = 0;

    //Task064-066
    private static void parseLKP(Node<Integer> root) {
        if (root != null) {
            parseLKP(root.getLeft());
            root.setValue(counter++);
            parseLKP(root.getRight());
        }
    }

    private static void parsePKL(Node<Integer> root) {
        if (root != null) {
            parsePKL(root.getRight());
            root.setValue(counter++);
            parsePKL(root.getLeft());
        }
    }

    private static void parseKLP(Node<Integer> root) {
        if (root != null) {
            root.setValue(counter++);
            parseKLP(root.getLeft());
            parseKLP(root.getRight());
        }
    }

    //Task061
    public void dsfMult(Node<Integer> root) {
        if (root != null) {
            mult *= root.getValue();
            dsfMult(root.getLeft());
            dsfMult(root.getRight());
        }
    }

    //Task060
    public void sumBFS_LP() {

        Queue<Node<Integer>> q = new LinkedList<>();
        q.offer(root);
        while (!q.isEmpty()) {
            Node<Integer> p = q.poll();
            sum+=p.getValue();
            if (p.getLeft() != null)
                q.offer(p.getLeft());
            if (p.getRight() != null)
                q.offer(p.getRight());
        }
    }

    //Task062
    public void maxKLP(Node root){
        if (root != null) {
            if((Integer) root.getValue()>max) {
                max =(Integer) root.getValue();
            }
            maxKLP(root.getLeft());
            maxKLP(root.getRight());
        }
    }


    public void enumerateAsBFS_LP() {

        Queue<Node<Integer>> q = new LinkedList<>();
        q.offer(root);
        counter = 0;
        while (!q.isEmpty()) {
            Node<Integer> p = q.poll();
            p.setValue(counter++);
            if (p.getLeft() != null)
                q.offer(p.getLeft());
            if (p.getRight() != null)
                q.offer(p.getRight());
        }
    }

    public void enumerateAsDFS_withStack() {

        Stack<Node<Integer>> q = new Stack<>();
        q.push(root);
        counter = 0;
        while (!q.isEmpty()) {
            Node<Integer> p = q.pop();
            p.setValue(counter++);
            if (p.getLeft() != null)
                q.push(p.getLeft());
            if (p.getRight() != null)
                q.push(p.getRight());
        }
    }

    public void enumerateAsKLP() {
        counter = 0;
        parseKLP(root);
    }

    public static void printNode(Node<Integer> root, int h) {
        if (root != null) {

            printNode(root.getRight(), h + 1);

            for (int i = 1; i <= h; i++)
                System.out.print("  ");
            System.out.println(root.getValue());


            printNode(root.getLeft(), h + 1);
        }
    }

    public static void main(String[] args) {
        BinaryIntTree bit = new BinaryIntTree(31);
        bit.enumerateAsBFS_LP();
        bit.printTree();

    }


}