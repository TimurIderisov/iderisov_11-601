package stackQueue;

/**
 * Created by Timur IDerisov 11-601 on 27.05.2017.
 */
public interface MyStack<T> {
    public void push(T value);
    public T pop();
    public boolean isEmpty();
}
