package stackQueue;

/**
 * Created by Timur IDerisov 11-601 on 27.05.2017.
 */
public class Elem<T> {


    public Elem(){}

    public Elem(T value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public Elem getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    private T value;
    private Elem next;
}
