package stackQueue;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class MyArrayQueue<T> implements MyQueue {
    int max = 100;
    int size = 0;
    T[] arr;

    @Override
    public boolean offer(Object o) {
        if (size < max) {
            if (size > 0)
                arr[size] = arr[size - 1];
            for (int i = size - 1; i >0; i--) {
                arr[i] =arr[i-1];
            }
            size++;
            return true;
        }
        return false;
    }

    @Override
    public Object poll() {
        size--;
        return arr[size + 1];
    }

    @Override
    public Object peek() {
        return arr[size];
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }
}
