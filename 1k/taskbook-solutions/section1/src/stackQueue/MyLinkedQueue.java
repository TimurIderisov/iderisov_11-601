package stackQueue;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */

public class MyLinkedQueue<T> implements MyQueue {
    int max = 100;
    int size = 0;
    Elem head;

    @Override
    public boolean offer(Object o) {
        if (size < max) {
            Elem p = head;
            for (int i = 0; i < size; i++) {
                p = p.getNext();
            }
            p.setValue(o);
            return true;
        } else return false;
    }

    @Override
    public Object poll() {
        Elem p = head;
        for (int i = 0; i < size; i++) {
            p = p.getNext();
        }
        size--;
        return p;
    }

    @Override
    public Object peek() {
        Elem p = head;
        for (int i = 0; i < size; i++) {
            p = p.getNext();
        }
        return p;
    }

    @Override
    public boolean isEmpty() {
        if (head == null) {
            return true;
        }
        return false;
    }
}
