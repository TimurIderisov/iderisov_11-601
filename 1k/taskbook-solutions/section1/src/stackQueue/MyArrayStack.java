package stackQueue;

/**
 * Created by Timur IDerisov 11-601 on 27.05.2017.
 */
public class MyArrayStack<T> implements MyStack {
    private final int CAPACITY = 1000;
    private int size = 0;
    private T[] arr = (T[]) new Object[CAPACITY];

    @Override
    public void push(Object value) {
        arr[size] = (T) value;
        size++;
    }

    @Override
    public Object pop() {
        T a = arr[size];
        size--;
        return a;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }
}
