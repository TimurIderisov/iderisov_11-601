package stackQueue;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class MyDoubleStackQueue<T> implements MyQueue {
    @Override
    public boolean offer(Object o) {
        return false;
    }

    @Override
    public Object poll() {
        return null;
    }

    @Override
    public Object peek() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
