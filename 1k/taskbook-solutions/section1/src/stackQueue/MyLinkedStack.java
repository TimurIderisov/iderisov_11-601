package stackQueue;

import linkedList.LinkedIntList;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Timur IDerisov 11-601 on 27.05.2017.
 */
public class MyLinkedStack<T> implements MyStack {
    private Elem head;

    @Override
    public void push(Object value) {
        head = new Elem(value, head);
    }

    @Override
    public T pop() {
        if (head == null) return null;
        T t = (T) head.getValue();
        head = head.getNext();
        return t;
    }

    public T peek(){
        if(head == null) return null;
        T t = (T) head.getValue();
        return t;
    }

    @Override
    public boolean isEmpty() {
        return (head == null);
    }

    //Task028
    public void reverseArray() {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter number of elements");
        int[] a = new int[sc.nextInt()];
        System.out.println("enter array");
        for (int i = 0; i < a.length; i++) {
            a[i] = sc.nextInt();
        }
        for (int c : a) {
            push(c);
        }
        for (int i = 0; i < a.length; i++) {
            a[i] = (Integer) pop();
        }
        for (int c : a) {
            System.out.println(c);
        }
    }

    //Task029
    public void bracketCheck() {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter string");
        String s = sc.nextLine();
        int counter = 100;
        Object c;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') push('(');
            if (s.charAt(i) == ')') {
                counter = i;
                c = pop();
                if (c == null) {
                    System.out.println("закрывающихся больше лиюо неверное соответствие. позиция " + i);
                    break;
                }
            }
        }

        if (pop() != null) {
            System.out.println("открывающих больше");
        }

    }
}
