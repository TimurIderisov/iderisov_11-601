package stackQueue;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public interface MyQueue<T> {
    public boolean offer(T t);
    public T poll();
    public T peek();
    public boolean isEmpty();
}
