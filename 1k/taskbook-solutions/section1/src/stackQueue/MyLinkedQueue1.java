package stackQueue;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class MyLinkedQueue1<T> implements java.util.Queue {
    int max = 100;
    int size = 0;
    T[] arr;

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        if (size < max) {
            arr[size] = (T) o;
            size++;
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean offer(Object o) {
        if (size < max) {
            arr[size] = (T) o;
            size++;
            return true;
        }
        return false;
    }

    @Override
    public Object remove() {
        size--;
        try {
            return arr[size + 1];
        } catch (Exception e ){throw e;}
    }

    @Override
    public Object poll() {
        size--;
        return arr[size + 1];
    }

    @Override
    public Object element() {
        try{
            return arr[size];
        } catch (Exception e){throw e;}
    }

    @Override
    public Object peek() {
        return arr[size];
    }
}
