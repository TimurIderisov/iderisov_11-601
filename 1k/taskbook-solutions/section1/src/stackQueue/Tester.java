package stackQueue;

import java.util.Scanner;

/**
 * Created by Timur IDerisov 11-601 on 27.05.2017.
 */
public class Tester {
    public static void main(String[] args) {
        MyLinkedStack ms = new MyLinkedStack();
        bracketCheck2();

    }

    //Task029a
    public static void bracketCheck2() {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter string");
        String s = sc.nextLine();
        int a = 0;
        int b = 0;
        char c = 'd';
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                a++;
                c = '(';
            }
            if (s.charAt(i) == ')') {
                b++;
                c = ')';
            }
        }
        if (a > b) System.out.println("открывающих больше");
        if (a < b) System.out.println("закрывающих больше");
        if (c == '(') System.out.println("неверное соответствие");
    }

}
