package linkedList;

public class IntElem {
    private int value;
    private IntElem next;

    public IntElem() {
    }

    public int getValue() {
        return value;
    }

    public IntElem getNext() {
        return next;
    }

    public IntElem(int c, IntElem next) {
        value = c;
        this.next = next;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setNext(IntElem next) {
        this.next = next;
    }
}
