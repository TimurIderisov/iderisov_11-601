package linkedList;

import java.util.Scanner;

public class Tester {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of elements");
        LinkedIntList ll = new LinkedIntList(sc.nextInt());
        ll.write();
        System.out.println("Enter the number of elements");
        LinkedIntList ll2 = new LinkedIntList(sc.nextInt());
        ll2.write();
        merging(ll,ll2);

        if (ll.hasZero() == true) {
            System.out.println("Has zero");
        } else {
            System.out.println("Doesn`t have zero");
        }
    }

    public static void shiftToHead1(LinkedIntList l) {
        IntElem m = l.getHead();
        l.setHead(m.getNext());
        IntElem p = l.getHead();
        while (p.getNext() != null) {
            p = p.getNext();
        }
        p.setNext(m);
        m.setNext(null);
    }

    //Task013
    public static LinkedIntList shiftToHead(LinkedIntList l, int t) {
        while (t > 0) {
            shiftToHead1(l);
            t--;
        }
        return l;
    }

    //Task012
    public static LinkedIntList shiftToEnd(LinkedIntList l, int t) {
        while (t > 0) {
            shiftToEnd1(l);
            t--;
        }
        return l;
    }

    public static void shiftToEnd1(LinkedIntList l) {
        IntElem p = l.getHead();
        while (p.getNext().getNext() != null) {
            p = p.getNext();
        }
        IntElem m = new IntElem();
        m.setValue(p.getNext().getValue());
        p.setNext(null);
        m.setNext(l.getHead());
        l.setHead(m);
    }

    //Task011
    public static LinkedIntList selectionSort(LinkedIntList lil, int a) {
        System.out.println("Ascending selectionSort");
        int j = 0;
        int id;
        int b;
        while (j <= a - 1) {
            IntElem p = lil.getHead();
            IntElem m = p;
            id = localMinSearch(lil, a, j);
            for (int i = 0; i < id; i++) {
                p = p.getNext();
            }
            for (int i = 0; i < j; i++) {
                m = m.getNext();
            }
            b = p.getValue();
            p.setValue(m.getValue());
            m.setValue(b);
            j++;
        }
        return lil;
    }

    public static int localMinSearch(LinkedIntList lil, int a, int j) {
        IntElem p = lil.getHead();
        for (int i = 0; i < j; i++) {
            p = p.getNext();
        }
        int min = p.getValue();
        int id = j;
        for (int i = j + 1; i < a; i++) {
            p = p.getNext();
            if (p.getValue() < min) {
                min = p.getValue();
                id = i;
            }
        }
        return id;
    }


    //Task010
    public static LinkedIntList bubbleSorting(LinkedIntList lil, int a) {
        System.out.println("Ascending bubbleSorting");
        IntElem ej = lil.getHead();
        int g;
        for (int i = 0; i < a - 1 && ej != null; i++) {
            for (int j = 0; j < a - i - 1 && ej != null; j++) {
                if (ej.getValue() > ej.getNext().getValue()) {
                    g = ej.getValue();
                    ej.setValue(ej.getNext().getValue());
                    ej.getNext().setValue(g);
                }
                ej = ej.getNext();
            }
            ej = lil.getHead();
        }
        return lil;
    }

    //Task018
    public static void add1(LinkedIntList li, int value, int place) {
        IntElem p = li.getHead();
        int c = 0;
        if (place == 0) {
            IntElem m = new IntElem(value, p);
            li.setHead(m);
        }
        while (p != null) {
            if (c == place - 1) {
                IntElem m = new IntElem(value, p.getNext());
                p.setNext(m);
            }
            p = p.getNext();
            c++;
        }
    }

    //Task021
    public static void deletingConcreteElem(LinkedIntList l) {
        int c = getValue();
        IntElem p = l.getHead();
        while (p.getNext() != null) {
            if (p.getNext().getNext() == null && p.getNext().getNext().getValue() == c) {
                p.getNext().setNext(null);
            }
            if (p.getNext().getValue() == c) {
                p.setNext(p.getNext().getNext());
            }
            p = p.getNext();
        }
        if (l.getHead().getValue() == c) {
            l.setHead(l.getHead().getNext());
        }
        output(l);
    }

    //Task023
    public static void deleteOnPos(LinkedIntList l) {
        int[] c = getPositions();
        for (int i = 0; i < c.length; i++) {
            deleteOnPosit(l, c[i]);
        }
        output(l);
    }

    private static void deleteOnPosit(LinkedIntList l, int t) {
        IntElem p = l.getHead();
        if (t == 0) {
            l.setHead(p.getNext());
        } else {
            for (int i = 1; i < t; i++) {
                p = p.getNext();
            }
            if (p.getNext() != null) p.setNext(p.getNext().getNext());
        }
    }


    public static int getValue() {
        System.out.println("ENTER NUMBER TO DELETE");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }


    public static void add(LinkedIntList li) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of adding elems");
        int k = sc.nextInt();
        for (int i = 0; i < k; i++) {
            System.out.println("Enter POSITION and VALUE");
            int p = sc.nextInt();
            int v = sc.nextInt();
            add1(li, v, p);
        }
        output(li);
    }

    public static LinkedIntList input(int a) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter list");
        LinkedIntList l = new LinkedIntList();
        for (int i = 0; i < a; i++) {
            IntElem p = new IntElem(sc.nextInt(), l.getHead());
            l.setHead(p);
        }
        return l;
    }


    public static void output(LinkedIntList l) {
        IntElem p = l.getHead();
        System.out.print("|");
        while (p != null) {
            System.out.print(p.getValue() + " | ");
            p = p.getNext();
        }
        System.out.println("");
    }

    //Task022
    public static void merging(LinkedIntList l1, LinkedIntList l2){
        IntElem p1= l1.getHead();
        IntElem p2 = l2.getHead();
        if(p1.getValue()<=p2.getValue()){
            while (p1.getNext()!=null&& p2!=null){
                if(p2.getValue()<=p1.getNext().getValue()) {
                    l2.setHead(p2.getNext());
                    p2.setNext(p1.getNext());
                    p1.setNext(p2);
                    p2 = l2.getHead();
                }
                p1 = p1.getNext();
            }
            if(p2!=null){
                p1.setNext(p2);
            }
            output(l1);
        } else{
            while (p2.getNext()!=null&& p1!=null){
                if(p1.getValue()<=p2.getNext().getValue()) {
                    l1.setHead(p1.getNext());
                    p1.setNext(p2.getNext());
                    p2.setNext(p1);
                    p1 = l1.getHead();
                }
                p2 = p2.getNext();
            }
            if(p1!=null){
                p2.setNext(p1);

            }
            output(l2);
        }
    }

    //Task025
    public static void convertBinary(LinkedIntList l) {
        IntElem p = l.getHead();
        int i = l.length() - 1;
        int s = 0;
        while (p != null) {
            if (p.getValue() == 1) {
                int k = 2;
                for (int j = 1; j < i; j++) {
                    k *= 2;
                }
                if (p.getNext() == null && p.getValue() == 1) k = 1;
                s += k;
            }
            i--;
            p = p.getNext();
        }
        LinkedIntList ll = new LinkedIntList();
        String st = Integer.toString(s);
        ll.setHead(new IntElem());
        p = ll.getHead();
        for (i = 0; i < st.length(); i++) {
            p.setValue((int) st.charAt(i) - 48);
            if (i != st.length() - 1) {
                p.setNext(new IntElem());
                p = p.getNext();
            }
        }
        ll.write();
    }

    public static int[] getPositions() {
        System.out.println("enter number of positions");
        Scanner sc = new Scanner(System.in);
        int d = sc.nextInt();
        int[] a = new int[d];
        System.out.println("Enter positions");
        for (int i = 0; i < a.length; i++) {
            a[i] = sc.nextInt();
        }
        return a;
    }
}
