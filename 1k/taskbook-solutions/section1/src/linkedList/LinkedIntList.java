package linkedList;

import java.util.Scanner;

public class LinkedIntList {
    private IntElem head;


    //Task 001
    public LinkedIntList(int numberOfElem) {
        System.out.println("Enter list");
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < numberOfElem; i++) {
            addTail(sc.nextInt());
        }
    }

    public LinkedIntList() {

    }

    //Task 002
    public int maxSearch() {
        IntElem p = head;
        int max = head.getValue();
        while (p != null) {
            if (p.getValue() > max) {
                max = p.getValue();
            }
            p = p.getNext();
        }
        return max;
    }

    //Task 003
    public int sum() {
        IntElem p = head;
        int s = 0;
        while (p != null) {
            s += p.getValue();
            p = p.getNext();
        }
        return s;
    }

    public int mult() {
        IntElem p = head;
        int s = 1;
        while (p != null) {
            s *= p.getValue();
            p = p.getNext();
        }
        return s;
    }

    //Task 004
    public int sumOfEven() {
        IntElem p = head;
        int s = 0;
        while (p != null) {
            if (p.getValue() % 2 == 0) {
                s += p.getValue();
            }
            p = p.getNext();
        }
        return s;
    }

    //Task004a
    public boolean hasZero() {
        IntElem p = head;
        while (p != null) {
            if (p.getValue() == 0) {
                return true;
            }
            p = p.getNext();
        }
        return false;
    }

    //Task 004b and Task 007
    public int mult2() {
        IntElem p = head;
        int s = 1;
        boolean flag = true;
        while (p != null) {
            if (flag) {
                s *= p.getValue();
                flag = false;
            } else {
                flag = true;
            }
            p = p.getNext();
        }
        return s;
    }

    //Task 005
    public void compaund(LinkedIntList l1) {
        IntElem p = head;
        while (p.getNext() != null) {
            p = p.getNext();
        }
        p.setNext(l1.getHead());
    }

    //Task 006
    public void changePosition() {
        IntElem p = head;
        int a = p.getValue();
        while (p.getNext() != null) {
            p = p.getNext();
        }
        head.setValue(p.getValue());
        p.setValue(a);
    }

    //Task008
    public int numberOfLocalMax() {
        IntElem p = head;
        int s = 0;
        while (p.getNext().getNext() != null) {
            if (p.getNext().getValue() > p.getValue() && p.getNext().getValue() > p.getNext().getNext().getValue()) {
                s++;
            }
            p = p.getNext();
        }
        return s;
    }

    public int numberOfLocalMin() {
        IntElem p = head;
        int s = 0;
        while (p.getNext().getNext() != null) {
            if (p.getNext().getValue() < p.getValue() && p.getNext().getValue() < p.getNext().getNext().getValue()) {
                s++;
            }
            p = p.getNext();
        }
        return s;
    }

    //Task 009
    public LinkedIntList(int a, boolean b) {
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < a; i++) {
            addHead(sc.nextInt());
        }
        System.out.println("reverse order");
        write();
    }

    //Task 014
    public void addingAfterEven() {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the int number to add after all even");
        int k = sc.nextInt();
        IntElem p = head;
        while (p.getNext() != null) {
            if (p.getValue() % 2 == 0) {
                IntElem m = new IntElem(k, p.getNext());
                p.setNext(m);
                p = m.getNext();
            } else {
                p = p.getNext();
            }
        }
        if (p.getValue() % 2 == 0) {
            p.setNext(new IntElem(k, null));
        }

        System.out.println("After adding number after all even");
        write();
    }

    //Task 015
    public void adding2or3() {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the int number to add after all %2 and %3");
        int k = sc.nextInt();
        IntElem p = head;
        while (p.getNext() != null) {
            if (p.getValue() % 2 == 0 || p.getValue() % 3 == 0) {
                IntElem m = new IntElem(k, p.getNext());
                p.setNext(m);
                p = m.getNext();
            } else {
                p = p.getNext();
            }
        }
        if (p.getValue() % 2 == 0 || p.getValue() % 3 == 0) {
            p.setNext(new IntElem(k, null));
        }

        System.out.println("After adding number after all even");
        write();
    }

    //Task 016
    public void addingBeforeEven() {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the int number to add before all even");
        int k = sc.nextInt();

        IntElem p = head;
        if (p.getValue() % 2 == 0) {
            IntElem m = new IntElem(k, p);
            setHead(m);
        }
        while (p.getNext().getNext() != null) {
            if (p.getNext().getValue() % 2 == 0) {
                IntElem m = new IntElem(k, p.getNext());
                p.setNext(m);
                p = m.getNext();
            } else {
                p = p.getNext();
            }
        }
        if (p.getNext().getValue() % 2 == 0) {
            IntElem m = new IntElem(k, p.getNext());
            p.setNext(m);
        }

        System.out.println("After adding number after all even");
        write();

    }

    //Task 017
    public void addingAroundPrimeNumber() {
        IntElem p = head;
        boolean isPrime = true;
        while (p.getNext().getNext() != null) {
            isPrime = true;
            int prev = 0;
            int next = 0;
            for (int i = 2; i < p.getNext().getValue() / 2; i++) {
                int val = p.getNext().getValue() % i;
                if (val == 0) {
                    isPrime = false;
                    break;
                }
            }
            prev = p.getNext().getValue() % 10;
            next = p.getNext().getValue();
            while (next > 9) {
                next = next / 10;
            }
            if (isPrime) {
                IntElem m1 = new IntElem(prev, p.getNext());
                p.setNext(m1);
                IntElem m2 = new IntElem(next, m1.getNext().getNext());
                m1.getNext().setNext(m2);
                p = m1.getNext().getNext();
            } else {
                p = p.getNext();
            }
        }
        System.out.println("After adding numbers around prime numbers");
        write();

    }

    //Task 019
    public void delete3last() {
        int c = 0;
        IntElem p = head;
        while (p != null) {
            c++;
            p = p.getNext();
        }
        p = head;
        if (c <= 3) {
            head = null;
        } else {
            while (p.getNext().getNext().getNext() != null) {
                if (p.getNext().getNext().getNext().getNext() == null) {
                    p.setNext(null);
                    break;
                } else {
                    p = p.getNext();
                }
            }
        }
        write();
    }

    //Task 020
    public void deleteNotEven() {
        IntElem p = head;
        while (p.getNext() != null) {
            if (p.getNext().getNext() == null) {
                if (p.getNext().getValue() % 2 == 1) {
                    p.setNext(null);
                    break;
                }
            }
            if (p.getNext().getValue() % 2 == 1) {
                p.setNext(p.getNext().getNext());
            }
            p = p.getNext();
        }
        if (head.getValue() % 2 == 1) {
            setHead(head.getNext());
        }
        write();
    }

    //Task024
    public void splitting() {
        LinkedIntList l1 = new LinkedIntList();
        IntElem p = head;
        l1.setHead(new IntElem());
        IntElem p1 = l1.getHead();
        while (p != null) {
            p1.setValue(p.getValue());

            p1.setNext(new IntElem());
            p = p.getNext();
            p1 = p1.getNext();

        }
        deleteNotEven();
        l1.deleteEven();
    }

    public void deleteEven() {
        IntElem p = head;
        while (p.getNext() != null) {
            if (p.getNext().getNext() == null) {
                if (p.getNext().getValue() % 2 == 0) {
                    p.setNext(null);
                    break;
                }
            }
            if (p.getNext().getValue() % 2 == 0) {
                p.setNext(p.getNext().getNext());
            }
            p = p.getNext();
        }
        if (head.getValue() % 2 == 0) {
            setHead(head.getNext());
        }
        write();
    }

    public int length() {
        IntElem p = head;
        int j = 0;
        while (p != null) {
            j++;
            p = p.getNext();
        }
        return j;
    }

    public void add(int x) {
        addHead(x);
    }


    public void addHead(int x) {
        IntElem p = new IntElem(x, head);
        head = p;
    }

    public void addTail(int x) {
        if (head != null) {
            IntElem p = head;
            IntElem m = new IntElem(x, null);
            while (
                    p != null) {
                if (p.getNext() == null) {
                    p.setNext(m);
                    break;
                }
                p = p.getNext();
            }
        } else {
            head = new IntElem(x, null);
        }
    }

    public void write() {
        IntElem p = head;
        while (p != null) {
            System.out.print(p.getValue() + " | ");
            p = p.getNext();
        }
        System.out.println("\n");

    }


    public IntElem getHead() {
        return head;
    }

    public void setHead(IntElem head) {
        this.head = head;
    }

}
