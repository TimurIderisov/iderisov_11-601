package threads;

import java.util.Scanner;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class Tester {
    public static void main(String[] args) throws InterruptedException {
       sum2();
    }

    //Task087
    public static void sum() throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of elements");
        int k = sc.nextInt();
        int[] a = new int[k];
        System.out.println("Enter array");
        for(int i = 0; i < k;i++)
            a[i] = sc.nextInt();
        Summator s1 = new Summator(a,0,k/2);
        Summator s2 = new Summator(a,k/2+1,k-1);
        s1.start();
        s2.start();
        s1.join();
        s2.join();
        System.out.println(s1.getS()+ s2.getS());
    }

    //Task088
    public static void sum2() throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.println("ENter k");
        int ch = sc.nextInt();
        System.out.println("Enter number of elements");
        int k = sc.nextInt();
        int[] a = new int[k];
        int sh = k/ ch;
        System.out.println("Enter array");
        for(int i = 0; i < k;i++)
            a[i] = sc.nextInt();
        int s = 0;
        for(int j =0; j< sh; j++) {
            Summator s1 = new Summator(a,sh*j,sh*(j+1));
            s1.start();
            s1.join();
            s+=s1.getS();
        }
        if(k - ch* sh> 0){
            Summator s1 = new Summator(a,sh*ch,k);
            s1.start();
            s1.join();
            s+=s1.getS();
        }
        System.out.println(s);
    }
}
