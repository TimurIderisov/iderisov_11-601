package threads;

/**
 * Created by Timur IDerisov 11-601 on 11.05.2017.
 */
public class Summator extends Thread {
    private int s = 0;
    public int getS(){

        return s;
    }
    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    private int[] arr;
    private int left,right;
    public Summator(int[] arr,int left,int right){
        this.arr = arr;
        this.left = left;
        this.right  = right;
    }
    public void run(){
        for(int i = left; i <= right; i++){
            s += arr[i];
        }
    }
}
