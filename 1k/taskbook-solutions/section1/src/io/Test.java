package io;

import java.io.*;

/**
 * Created by Timur IDerisov 11-601 on 29.05.2017.
 */
public class Test {
    public static void main(String[] args) throws IOException {
        copyPaste();

    }

    //Task084
    public static void copyPaste() throws IOException {

        FileReader fr = new FileReader(new File("src\\io\\in.txt"));
        FileWriter fw = new FileWriter(new File("src\\io\\out.txt"));
        long f = System.nanoTime();
        while (fr.ready()) {
            fw.write((char)fr.read());
        }
        fw.close();
        System.out.println(f = System.nanoTime() - f);

        BufferedReader br = new BufferedReader(fr);
        BufferedWriter bw = new BufferedWriter(fw);
        f = System.nanoTime();
        while (br.ready()){
            bw.write(br.readLine());
        }
        System.out.println(System.nanoTime()-f);
    }


}
