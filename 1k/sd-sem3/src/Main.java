/**
 * Created by Timur IDerisov 11-601 on 07.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        int[] a = {4, 2, 5, 7, 8, 9, 0, 10};
        Tree coolTree = new Tree(a);
        coolTree.build(a, 1, 0, a.length - 1);
        System.out.println(coolTree.sum(1, 0, a.length - 1, 0, 3));
        coolTree.update(1, 0, a.length - 1, 0, 10);
    }
}
