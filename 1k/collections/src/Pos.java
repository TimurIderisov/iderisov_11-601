public class Pos implements Comparable {
    private int freq;

    public int getFreq() {
        return freq;
    }

    public void setFreq(int freq) {
        this.freq = freq;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    private String word;

    public String toString() {
        String s = freq + " " + word;
        return s;
    }

    public int compareTo(Object pos) {
        Pos a = (Pos) pos;
        if (freq > a.getFreq()) {
            return 1;
        } else if (freq < a.getFreq()) {
            return -1;
        } else if (freq == a.getFreq()) {
            return 0;
        }
        return 0;
    }
}
