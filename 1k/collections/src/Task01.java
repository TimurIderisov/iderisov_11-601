import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import  java.io.*;
public class Task01 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("C://bbb//collections//src/en_v1.dic"));
        Map<String,Integer> m = new HashMap<String,Integer>();
        do{
            String st = sc.nextLine();
            String[] arr = st.split(" ");
            m.put(arr[0], Integer.parseInt(arr[1]));
        }
        while (sc.hasNext());
        System.out.println(m);
    }
}
