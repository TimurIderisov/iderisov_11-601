package Base.entities;

public class Continent {
    public Continent() {

    }

    public int getContId() {
        return contId;
    }

    public void setContId(int contId) {
        this.contId = contId;
    }


    private int contId;

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public Continent(int contId, String continent) {
        this.contId = contId;
        this.continent = continent;
    }

    @Override
    public String toString() {
        return "Continents{" +
                "contId=" + contId +
                ", continent='" + continent + '\'' +
                '}';
    }

    private String continent;
}
