package Base.entities;

public class Country {
    private int countryId;

    public Country(int countryId, String countryName, int continents) {
        this.countryId = countryId;
        this.countryName = countryName;
        this.continents = continents;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryId=" + countryId +
                ", countryName='" + countryName + '\'' +
                ", continents=" + continents +
                ", continent=" + continent +
                '}';
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getContinents() {
        return continents;
    }

    public void setContinents(int continents) {
        this.continents = continents;
    }

    private String countryName;
    private int continents;
    private Continent continent;
}
