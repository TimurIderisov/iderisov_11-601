package Base.db;

import Base.entities.Continent;
import Base.entities.Country;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Database {
    private List<Country> countries;
    private List<Continent> continents;

    public void init() {
        prepareContinents("datasets/continents.csv");
        prepareCountries("datasets/countries.csv");

    }

    private void prepareContinents(String filename) {
        continents = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(filename));
            while (sc.hasNext()) {
                String[] strData = sc.nextLine().split(",");
                Continent c = new Continent();
                c.setContId(Integer.parseInt(strData[0]));
                c.setContinent(strData[1].substring(1,strData[1].length() -1));
                continents.add(c);
            }
            sc.close();
            System.out.println("Continents Data was loaded");
        } catch (IOException e) {
            System.out.println("file not found, collection is empty");
        }
    }

    private void prepareCountries(String filename) {
        countries = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(filename));
            while (sc.hasNext()) {
                String[] str;
            }
        } catch (IOException e) {
            System.out.println("file not found, collection is empty");
        }
    }

}
