import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Task02 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("C://bbb//collections//src/en_v1.dic"));
        Map<Integer, Set<String>> m = new TreeMap<>();
        do {
            String st = sc.nextLine();
            String[] arr = st.split(" ");
            Integer req = Integer.parseInt(arr[1]);
            if (m.containsKey(req)) {
                m.get(req).add(arr[0]);
            } else {
                Set<String> setStr = new HashSet<>();
                setStr.add(arr[0]);
                m.put(req, setStr);
            }
        }
        while (sc.hasNext());
        System.out.println(m);
    }
}