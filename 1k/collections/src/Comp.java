import java.util.Comparator;

class Comp implements Comparator<Pos>
{
    public int compare(Pos obj1, Pos obj2)
    {
        return obj1.compareTo(obj2);
    }
}