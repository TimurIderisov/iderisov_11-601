import java.util.ArrayList;
import java.util.stream.Collectors;

public class Task0 {
    public static void main(String[] args) {
        ArrayList<People> peoples = new ArrayList();

        peoples.stream().filter((p)-> p.getAge() >= 18 && p.getAge() < 27
                && p.getSex() == true).collect(Collectors.toList());

        peoples.stream().filter((p) -> p.getSex() == true).
                mapToInt(People::getAge).average().getAsDouble();

        peoples.stream().filter((p) -> p.getAge() >= 18).filter(
                (p) -> (p.getSex() == false && p.getAge() < 55) || (p.getSex() == true && p.getAge() < 60)).count();

    }
}
