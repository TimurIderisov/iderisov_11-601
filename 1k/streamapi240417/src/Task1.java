import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

public class Task1 {
    public static void main(String[] args) {
        Collection ordered = Arrays.asList(1,2,3,4,5);
        Collection nonOrdered = new HashSet<>(ordered);
        System.out.println(nonOrdered.stream().distinct().collect(Collectors.toList()));
        System.out.println(ordered.stream().distinct().collect(Collectors.toList()));
        System.out.println(ordered.stream().map((s) -> s + "_1").collect(Collectors.toList()));

    }
}
