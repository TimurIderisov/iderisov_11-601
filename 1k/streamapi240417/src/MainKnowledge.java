import java.util.ArrayList;

public class MainKnowledge {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList(10);
        for(int i = 0; i< 10;i++){
            a.add(i*119);
        }
        int k = a.stream().filter(o -> o % 2 != 0).reduce((s1, s2) -> s1 + s2).orElse(0);
        System.out.println(k);
        k = (int) a.stream().filter(o -> o % 119 == 0).count();
        System.out.println(k);
        k = a.stream().skip(4).findFirst().orElse(100);
        System.out.println(k);
    }
}
