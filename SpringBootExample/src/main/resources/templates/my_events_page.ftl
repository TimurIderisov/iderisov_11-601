<#ftl encoding='UTF-8'>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <STYLE>
        body {
            background-image: url("https://lh3.googleusercontent.com/5Vk3gQ9Tx31JapQMeILsyimryOey0jWk_rFJ9lE85CNBHScjUNQy5nhcPoNYTBlgUA=h556");
            background-position: center;
            background-size: cover;
        }
        #container {
            background: #DDD;
            max-width: 1400px;
        }

        .item {
            width: 200px;
            float: left;
        }

        .item img {
            display: block;
            width: 100%;
        }

        button {
            font-size: 18px;
        }

        .container {
            width: 100%;
        }
    </STYLE>
    <script src="//masonry.desandro.com/masonry.pkgd.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.0.4/jquery.imagesloaded.js"></script>
</head>
<body>


<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script>
    function sendFile(file) {
        var formData = new FormData();
        formData.append("file", file);
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/files", true);
        xhr.send(formData,);
    }
</script>
<script>
    $(function () {

        var $container = $('#container').masonry({
            itemSelector: '.item',
            columnWidth: 200
        });

        // reveal initial images
        $container.masonryImagesReveal($('#images').find('.item'));
    });

    $.fn.masonryImagesReveal = function ($items) {
        var msnry = this.data('masonry');
        var itemSelector = msnry.options.itemSelector;
        // hide by default
        $items.hide();
        // append to container
        this.append($items);
        $items.imagesLoaded().progress(function (imgLoad, image) {
            // get item
            // image is imagesLoaded class, not <img>, <img> is image.img
            var $item = $(image.img).parents(itemSelector);
            // un-hide item
            $item.show();
            // masonry does its thing
            msnry.appended($item);
        });

        return this;
    };
</script>


<a href="/user/profile" style="margin-left: 43%;margin-bottom: 5px;margin-top: 5px; width: 200px; text-align: center" class="btn btn-lg btn-success btn-block">Profile</a>
<div>
    <input type="file" id="file" name="file" class="btn btn-lg btn-success btn-block" style="width: 500px; margin: 5px" placeholder="Имя файла..."/>
    <button style="margin: 5px" onclick="sendFile(($('#file'))[0]['files'][0])"
            class="btn btn-primary">Загрузить файл
    </button>
    <input type="hidden" id="file_hidden">
    <div class="filename"></div>
</div>



<br><br>

<div class="container">
    <div id="images">
    <#list model.fileInfos as fileInfo>
        <div class="item">
            <img src="/files/${fileInfo.storageFileName}">
        </div>
    </#list>
    </div>
</div>

</body>