<#ftl encoding='UTF-8'>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <STYLE>
        body {
            background-image: url("https://lh3.googleusercontent.com/5Vk3gQ9Tx31JapQMeILsyimryOey0jWk_rFJ9lE85CNBHScjUNQy5nhcPoNYTBlgUA=h556");
            background-position: center;
            background-size: cover;
        }

        .container {
            margin-top: 200px;
        }
    </STYLE>
</head>
<body>
<#if model.error.isPresent()>
<div class="alert alert-danger" role="alert">Проверьте правильность логина и пароля, а также статус аккаунта(подтверждение по e-mail)</div>
</#if>


<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please sign in</h3>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" action="/login" method="post">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Login" name="login" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password"
                                       value="">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="remember-me" type="checkbox"> Remember Me
                                </label>
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                        </fieldset>
                    </form>
                    <a href="/signUp"><button style="text-decoration: no-underline !important;margin-top: 5px; background-color: #3e94ec" class="btn btn-lg btn-success btn-block" >Sign Up</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

</body>