<#ftl encoding='UTF-8'>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <STYLE>
        body {
            background-image: url("https://lh3.googleusercontent.com/5Vk3gQ9Tx31JapQMeILsyimryOey0jWk_rFJ9lE85CNBHScjUNQy5nhcPoNYTBlgUA=h556");
            background-position: center;
            background-size: cover;
        }
    </STYLE>
</head>
<body><br><br>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form accept-charset="UTF-8" action="/signUp" method="post">
                        <fieldset>
                                <h2>Регистрация успешна</h2>
                                <a href="/login" style="margin: 5px" class="btn btn-lg btn-success btn-block">Log in</a>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>