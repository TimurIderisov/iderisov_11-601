<#ftl encoding='UTF-8'>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <STYLE>
        body {
            background-image: url("https://lh3.googleusercontent.com/5Vk3gQ9Tx31JapQMeILsyimryOey0jWk_rFJ9lE85CNBHScjUNQy5nhcPoNYTBlgUA=h556");
            background-position: center;
            background-size: cover;
        }
    </STYLE>
</head>
<body><br><br>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form accept-charset="UTF-8" action="/signUp" method="post">
                        <fieldset>
                            <h2>
                                <h1 style="text-align: center">${model.user.login}</h1>

                                <div   style="text-align:center; width: auto; height: 200px; overflow: hidden;">
                                    <img class="img-rounded" style=" align-content: center;height: 200px; width: auto" src="https://all-psd.ru/uploads/posts/2011-05/home-icon.jpg">
                                </div>
                                <a href="/events" style="margin: 5px" class="btn btn-lg btn-success btn-block">Events</a>
                                <a href="/my-events" style="margin: 5px" class="btn btn-lg btn-success btn-block">Album</a>
                                <a href="/logout" style="margin: 5px" class="btn btn-lg btn-success btn-block">Log out</a>
                            </h2>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>