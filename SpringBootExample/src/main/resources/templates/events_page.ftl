<#ftl encoding='UTF-8'>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <STYLE>
        body {
            background-image: url("https://lh3.googleusercontent.com/5Vk3gQ9Tx31JapQMeILsyimryOey0jWk_rFJ9lE85CNBHScjUNQy5nhcPoNYTBlgUA=h556");
            background-position: center;
            background-size: cover;
            background-attachment: fixed;
            scroll-behavior: smooth;
            overflow-x: hidden;
        }
    </STYLE>
    <link href="/css/style.css" rel="stylesheet"/>
    <link href="/bootstrap/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="/bootstrap/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
</head>
<body>

<div class="row">
    <div class="col-md-3">
        <a href="/event/"
           style="margin-left: 44%;margin-bottom: 5px;margin-top: 5px; width: 200px; text-align: center"
           class="btn btn-lg btn-success btn-block">Add Event</a>
    </div>
    <div class="col=md-3">
        <a href="/user/profile"
           style="margin-left: 75%;margin-bottom: 5px;margin-top: 5px; width: 200px; text-align: center"
           class="btn btn-lg btn-success btn-block">Profile</a>
    </div>
</div>


<#list model.events as event>

<div style="width: 80%; height: 300px; background-color: white; opacity: 20; margin-left: 10%; margin-top:20px;"
     class="row">

    <div class="col-md-6">

        <div style="font-family: Consolas">
        ${event.author.login}
        </div>

        <img style="width: 95%; padding-top: 10px; max-height: 270px"
             src="/files/${event.eventPhoto.fileInfo.storageFileName}">

    </div>

    <div class="col-md-5" style="margin-right: 10px; height: 300px">

        <h2>${event.description}</h2>

        <br>

        <div style="">
        <button>like</button>
        </div>



    </div>


</div>

</#list>


</body>