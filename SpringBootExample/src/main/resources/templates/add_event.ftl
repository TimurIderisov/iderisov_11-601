<#ftl encoding='UTF-8'>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <STYLE>
        body {
            background-image: url("https://lh3.googleusercontent.com/5Vk3gQ9Tx31JapQMeILsyimryOey0jWk_rFJ9lE85CNBHScjUNQy5nhcPoNYTBlgUA=h556");
            background-position: center;
            background-size: cover;
            background-attachment: fixed;
            scroll-behavior: smooth;
            overflow-x: hidden;
        }
    </STYLE>
    <link href="/bootstrap/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="/bootstrap/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
</head>
<body>

<div class="row">
    <div class="col-md-3">
        <a href="/events/"
           style="margin-left: 44%;margin-bottom: 5px;margin-top: 5px; width: 200px; text-align: center"
           class="btn btn-lg btn-success btn-block">Events</a>
    </div>
    <div class="col=md-3">
        <a href="/user/profile"
           style="margin-left: 75%;margin-bottom: 5px;margin-top: 5px; width: 200px; text-align: center"
           class="btn btn-lg btn-success btn-block">Profile</a>
    </div>
</div>

<h2 style="text-align: center; color: white;">Offer your event!</h2>
<br><br>


<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/events" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" type="text" name="description" placeholder="description">
                            </div>

                            <div class="form-group">
                                <input id="file" type="file" name="photo" accept="image/*"/>
                            </div>

                            <br>

                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Post">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>