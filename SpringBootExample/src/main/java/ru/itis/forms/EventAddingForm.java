package ru.itis.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Timur Iderisov on 13.05.2018.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EventAddingForm {
    private String description;
    private MultipartFile photo;
}
