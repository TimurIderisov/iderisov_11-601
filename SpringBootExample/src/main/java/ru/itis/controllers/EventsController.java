package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.forms.EventAddingForm;
import ru.itis.models.Event;
import ru.itis.repositories.FileInfoRepository;
import ru.itis.services.EventService;
import ru.itis.validators.EventAddingFormValidator;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class EventsController {

    @Autowired
    private EventService eventService;

    @Autowired
    private FileInfoRepository fileInfoRepository;



    @Autowired
    private EventAddingFormValidator eventAddingFormValidator;


    @InitBinder("eventAddingForm")
    public void initEventFormValidator(WebDataBinder binder) {
        binder.addValidators(eventAddingFormValidator);
    }

    @GetMapping("/events")
    public String getEventsPage(@ModelAttribute("model")ModelMap model) {

        List<Event> events = eventService.findAll();
        model.addAttribute("events", events);
        return "events_page";

    }

    @GetMapping("/my-events")
    public String getFileList(@ModelAttribute("model") ModelMap model) {
        List<Event> events = eventService.findAllByAuthor();
        model.addAttribute("events", events);
        return "events_page";
    }

    @PostMapping("/events")
    public String addNewEvent(RedirectAttributes redirectAttributes,
                              @Valid @ModelAttribute("eventAddingForm") EventAddingForm eventAddingForm,
                              HttpServletRequest req,
                              @ModelAttribute("model")ModelMap model,
                              BindingResult errors) {
        eventService.save(eventAddingForm);
        return "redirect:/events";
    }



    @GetMapping("/event")
    public String addEventPage(){
        return "add_event";
    }
}
