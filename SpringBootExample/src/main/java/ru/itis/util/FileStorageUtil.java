package ru.itis.util;

import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.models.EventPhoto;
import ru.itis.models.FileInfo;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Component
public class FileStorageUtil {

    @Value("${storage.path}")
    private String eventPhotoStoragePath;

    @Value("${storage.path}")
    private String storagePath;

    public String getStoragePath() {
        return storagePath;
    }

    @SneakyThrows
    public void copyToStorage(MultipartFile file, String storageFileName) {
        Files.copy(file.getInputStream(), Paths.get(storagePath, storageFileName));
    }

    public FileInfo convertFromMultipart(MultipartFile file) {
        // получаем название файла
        String originalFileName = file.getOriginalFilename();
        // вытаскиваем контент-тайп
        String type = file.getContentType();
        // размер файла
        long size = file.getSize();
        // создаем имя файла
        String storageName = createStorageName(originalFileName);
        // получаем url файла по которому он будет доступен внутри системы
        String fileUrl = getUrlOfFile(storageName);
        return FileInfo.builder()
                .originalFileName(originalFileName)
                .storageFileName(storageName)
                .url(fileUrl)
                .size(size)
                .type(type)
                .build();
    }

    private String getUrlOfFile(String storageFileName) {
        return storagePath + "/" + storageFileName;
    }

    private String createStorageName(String originalFileName) {
        String extension = FilenameUtils.getExtension(originalFileName);
        String newFileName = UUID.randomUUID().toString();
        return newFileName + "." + extension;
    }

    public FileInfo getEventPhotoByMultipart(MultipartFile file) {
        FileInfo fileInfo = convertFromMultipart(file);
        fileInfo.setUrl(getFullPathOfEventPhoto(fileInfo.getStorageFileName()));
        return fileInfo;
    }

    private String getFullPathOfEventPhoto(String storageName) {
        return eventPhotoStoragePath + "/" + storageName;
    }

    @SneakyThrows
    public void saveEventPhotoToStorage(MultipartFile file, EventPhoto eventPhoto) {
        Files.copy(file.getInputStream(), Paths.get(eventPhotoStoragePath, eventPhoto.getFileInfo().getStorageFileName()));
    }

}