package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Event;
import ru.itis.models.User;

import java.util.List;

public interface EventsRepository extends JpaRepository<Event,Integer>{
    List<Event> findAllByAuthor(User author);
}
