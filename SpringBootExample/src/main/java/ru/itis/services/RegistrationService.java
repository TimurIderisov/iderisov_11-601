package ru.itis.services;

import ru.itis.forms.UserRegistrationForm;

public interface RegistrationService {
    void register(UserRegistrationForm userForm);

}
