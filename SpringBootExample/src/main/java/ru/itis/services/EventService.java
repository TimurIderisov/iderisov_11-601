package ru.itis.services;

import ru.itis.forms.EventAddingForm;
import ru.itis.models.Event;
import ru.itis.models.User;

import java.util.List;
import java.util.Optional;

/**
 * Created by Timur Iderisov on 13.05.2018.
 */
public interface EventService {
    List<Event> findAll();
    List<Event> findAllByAuthor();
    void save(EventAddingForm eventAddingForm);
    void setUser(Optional<User> userl);

}
