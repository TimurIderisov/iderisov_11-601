package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.forms.EventAddingForm;
import ru.itis.models.Event;
import ru.itis.models.EventPhoto;
import ru.itis.models.FileInfo;
import ru.itis.models.User;
import ru.itis.repositories.EventsRepository;
import ru.itis.repositories.UsersRepository;
import ru.itis.util.FileStorageUtil;

import java.util.List;
import java.util.Optional;

/**
 * Created by Timur Iderisov on 13.05.2018.
 */
@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private FileStorageUtil fileStorageUtil;

    @Autowired
    private EventsRepository eventsRepository;


    @Autowired
    private UsersRepository usersRepository;

    public void setUser(Optional<User> user){
        this.user =user;
    }

    private static Optional<User> user;


    @Override
    public List<Event> findAll() {
        return eventsRepository.findAll();
    }

    @Override
    public List<Event> findAllByAuthor() {
        return eventsRepository.findAllByAuthor(user.get());
    }

    @Override
    public void save(EventAddingForm eventAddingForm) {
        FileInfo photoFileInfo = fileStorageUtil.getEventPhotoByMultipart(eventAddingForm.getPhoto());
        EventPhoto eventPhoto = EventPhoto.builder().fileInfo(photoFileInfo).build();
        fileStorageUtil.saveEventPhotoToStorage(eventAddingForm.getPhoto(), eventPhoto);

        eventsRepository.save(Event.builder()
                .description(eventAddingForm.getDescription())
                .eventPhoto(eventPhoto)
                .author(user.get())
                .build()
        );

    }

}
