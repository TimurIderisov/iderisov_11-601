package ru.itis.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.itis.forms.EventAddingForm;

/**
 * Created by Timur Iderisov on 13.05.2018.
 */
@Component
public class EventAddingFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(EventAddingForm.class.getName());
    }

    @Override
    public void validate(Object o, Errors errors) {
        //TODO
    }
}
