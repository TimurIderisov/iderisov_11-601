package ru.itis.models;

import lombok.*;
import ru.itis.security.role.Role;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "owner")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(unique = true)
    private String login;

    private String hashPassword;

   @Enumerated(EnumType.STRING)
    private Role role;



}
