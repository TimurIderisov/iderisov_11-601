package messenger;

import java.io.*;
import java.net.Socket;

public class Client {

    public static void main(String[] args) {
            int port = 1234;
            String host = "localhost";
            Socket s = null;
            try {
                s = new Socket(host, port);
                InputStream is = s.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

                System.out.println(bufferedReader.readLine());

            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
