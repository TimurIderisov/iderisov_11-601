package messenger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class ChatClient {
    public static void main(String[] args) {
        int port = 9090;
        String host = "localhost";
        Socket s = null;
        try {
            s = new Socket(host, port);
            InputStream is = s.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            while(true) {
                System.out.println(bufferedReader.readLine());

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}