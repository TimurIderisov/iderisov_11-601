package messenger;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    public static void main(String[] args) {
        final int PORT = 1234;
        ServerSocket s = null;
        PrintWriter writer;

        try {
            s = new ServerSocket(PORT);
            Socket client = s.accept();
            Scanner sc = new Scanner(System.in);
            while (true) {
                String st = sc.next();
                writer = new PrintWriter(client.getOutputStream());
                writer.println(st);
                writer.flush();
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
