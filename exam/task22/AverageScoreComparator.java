import java.util.Comparator;

public class AverageScoreComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        return getCoeff(o2) - getCoeff(o1);
    }


    private int getCoeff(int number) {

        if (number < 0) {
            number = -number;
        }

        int sum = 0;
        int mult = 1;
        int numb;

        while (number > 0) {
            numb = number % 10;
            number /= 10;
            sum += numb;
            mult *= numb;
        }

        int k = Math.abs(sum - mult);
        return k;
    }
}