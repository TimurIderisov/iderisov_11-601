import java.util.*;

/**
 * Created by Timur Iderisov on 16.06.2018.
 */
public class Main {
    //Task22

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter number of elements");

        Integer length = sc.nextInt();

		System.out.println("Enter elements");

        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < length; i++){
            numbers.add(sc.nextInt());
        }

        Collections.sort(numbers,new AverageScoreComparator());

        System.out.println(numbers);

    }

}
