/**
 * Created by Timur Iderisov on 16.06.2018.
 */
public class Actor {
    private int rate;
    private String name;

    public Actor() {
        this.rate = 0;
    }

    public Actor(String name) {
        this();
        this.name = name;
    }


    public void represent() {
        System.out.println("my name is " + name);
    }

    public void sayHello() {
        System.out.println("hello");
    }

    public void sayBye() {
        System.out.println("bye");
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public int getRate() {
        return rate;
    }

}
