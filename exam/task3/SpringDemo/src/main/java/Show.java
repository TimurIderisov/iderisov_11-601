import java.util.List;

/**
 * Created by Timur Iderisov on 16.06.2018.
 */
public class Show {
    private int rate;
    private String name;
    private List<Object> artists;

    public Show() {
        this.rate = 0;
    }

    public Show(String name) {
        this();
        this.name = name;
    }

    public void startShow() {
        System.out.println("start show");
    }

    public void endShow() {
        System.out.println("end show");
    }

    public void stopShow() {
        System.out.println("stop show");
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public int getRate() {
        return rate;
    }

}
