/**
 * Created by Timur Iderisov on 16.06.2018.
 */
public interface SongProducer {
    void produceSong();
}
