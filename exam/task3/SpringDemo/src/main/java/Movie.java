import java.util.List;

/**
 * Created by Timur Iderisov on 16.06.2018.
 */
public class Movie extends Show {
    private List<Actor> artists;

    public Movie() {
        super();
    }

    public Movie(String name) {
        super(name);
    }


    public void startShow() {
        System.out.println("start movie");
    }

    public void endShow() {
        System.out.println("end movie");
    }

    public void stopShow() {
        System.out.println("stop movie");
    }
}
