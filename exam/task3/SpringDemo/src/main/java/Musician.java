/**
 * Created by Timur Iderisov on 16.06.2018.
 */
public class Musician implements Perform, SongProducer{
    private int rate;
    private String name;

    public Musician(){
        this.rate = 0;
    }

    public Musician(String name){
        this();
        this.name = name;
    }

    @Override
    public void perform() {
        System.out.println("I am performing");
    }

    @Override
    public void produceSong() {
        System.out.println("I am producing song");
    }

    public void represent(){
        System.out.println("my name is " + name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public int getRate() {
        return rate;
    }

}
