import java.util.List;

/**
 * Created by Timur Iderisov on 16.06.2018.
 */
public class Concert extends Show {
    private List<Musician> artists;

    public Concert() {
        super();
    }

    public Concert(String name) {
        super(name);
    }


    public void startShow() {
        System.out.println("start concert");
    }

    public void endShow() {
        System.out.println("end concert");
    }

    public void stopShow() {
        System.out.println("stop concert");
    }

}
