import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");

        Actor actor = context.getBean(Actor.class);
        actor.sayHello();
        actor.represent();
        actor.sayBye();

        Musician musician = context.getBean(Musician.class);
        musician.represent();
        musician.produceSong();
        musician.perform();

        Show show = (Show) context.getBean("show");
        show.startShow();
        show.stopShow();
        show.endShow();

        Movie movie = context.getBean(Movie.class);
        movie.startShow();
        movie.stopShow();
        movie.endShow();

        Concert concert = context.getBean(Concert.class);
        concert.startShow();
        concert.stopShow();
        concert.endShow();

    }
}
