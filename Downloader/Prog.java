import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Prog {
    public static void main(String[] args) {



        URL url = null;
        try {
            url = new URL("http://trinixy.ru/89413-desyatka-samyh-strannyh-snimkov-v-mire-21-foto.html");
            LineNumberReader r = new LineNumberReader(new InputStreamReader(url.openStream()));
            String s = r.readLine();
            int c = 0;
            while (s != null) {
                if(checkWithRegExp(s)){
                    Pattern p = Pattern.compile("http[\\s\\S]*.jpg");
                    Matcher m = p.matcher(s);
                    if(m.find()){

                        s= m.group(0);
                        downloading(s, c);
                        c++;
                    }
                }
                s = r.readLine();
            }
            r.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void downloading(String urlString, int c){
        URL url = null;
        try {
            url = new URL(
                    urlString
            );

            InputStream s1 = new BufferedInputStream(
                    url.openStream());
            java.io.FileOutputStream f1 =
                    new FileOutputStream(
                            new File("web/download/"+c+".jpg"));
            int a = s1.read();
            while (a != -1) {
                f1.write(a);
                a = s1.read();
            }
            s1.close();
            f1.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static boolean checkWithRegExp(String string){
        Pattern p = Pattern.compile("[\\s\\S]*src=\"[\\s\\S]*.jpg[\\s\\S]*");
        Matcher m = p.matcher(string);
        return m.matches();
    }

}
