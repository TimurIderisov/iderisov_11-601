package sample.client;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.SubScene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import sample.client.Player;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;

public class TicTacToeController {
    @FXML
    Label score;
    @FXML
    Button b1;
    @FXML
    Button b2;
    @FXML
    Button b3;
    @FXML
    Button b4;
    @FXML
    Button b5;
    @FXML
    Button b6;
    @FXML
    Button b7;
    @FXML
    Button b8;
    @FXML
    Button b9;
    @FXML
    GridPane gameBoard;

    private Long time;
    private Player player;
    private ArrayList<Button> buttons = new ArrayList<>();
    private final int port = 1234;
    private String host;

    @FXML
    private void initialize() {
        addButtons(new Button[]{b1, b2, b3, b4, b5, b6, b7, b8, b9});
        getRandBut();
        player = new Player();
        score.textProperty().bind(player.getScore().asString());
        startingDialogs();
        time = System.currentTimeMillis();
    }

    public void buttonClickHandler(ActionEvent evt) {
        Button clickedButton = (Button) evt.getTarget();
        String buttonLabel = clickedButton.getText();
        if ("Tomato".equals(buttonLabel)) {
            clickedButton.setText("");
            player.incScore();
        }
        if (System.currentTimeMillis() - time > 10000) {
            throwAlert();
            time = System.currentTimeMillis();
            sendResult();
            player.reboot();
        }
        getRandBut();
    }


    public void menuClickHandler(ActionEvent evt) {
        MenuItem clickedMenu = (MenuItem) evt.getTarget();
        String menuLabel = clickedMenu.getText();
        if ("Top-table".equals(menuLabel)) {
           getResults();
        }
    }

    private void getResults() {
        Socket s = null;
        Long delta = System.currentTimeMillis() - time;
        try {
            s = new Socket(host, port);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String string = bufferedReader.readLine();
            Alert alert = new Alert(Alert.AlertType.INFORMATION,
                    string
            );
            alert.setTitle("TOP RESULTS");
            alert.setHeaderText("TOP-Players: ");
            alert.showAndWait();
            time = System.currentTimeMillis() - delta;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendResult() {
        try {
            Socket s = new Socket(host, port);
            PrintWriter writer = new PrintWriter(s.getOutputStream());
            writer.write(player.getScore().get() + " " + player.getName().toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startingDialogs() {
        TextInputDialog dialog = new TextInputDialog("ipv4");
        dialog.setTitle("Starting...");
        dialog.setHeaderText("HOST IP");
        dialog.setContentText("Please enter ip address");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            host = result.get();
        }

        TextInputDialog dialog2 = new TextInputDialog("ipv4");
        dialog.setTitle("Starting...");
        dialog.setHeaderText("10 sec. Just you, your knife and TOMATOES");
        dialog.setContentText("Please enter your nickname");
        Optional<String> name = dialog.showAndWait();
        if (result.isPresent()) {
            player.setName(name.get());
        }
//        Alert alert = new Alert(Alert.AlertType.INFORMATION, "10 sec. Just you, your knife and TOMATOES");
//        alert.setHeaderText("Rules are simple..");
//        alert.setTitle("Rules");
//        alert.showAndWait();
    }

    private void getRandBut() {
        for (Button b : buttons) {
            b.setText("");
            b.setStyle("-fx-background-color: black");
        }
        Button b = buttons.get((int) (Math.random() * 9));
        b.setText("Tomato");
        b.setStyle("-fx-background-color: red");
    }

    private void addButtons(Button[] bs) {
        for (Button b : bs) {
            buttons.add(b);
        }
    }

    private void throwAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Prepare and go");
        alert.setTitle("Time out");
        alert.setHeaderText("your score is " + score.textProperty().getValue());
        alert.showAndWait();
    }

}