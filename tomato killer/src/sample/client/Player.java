package sample.client;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
public class Player {
    private IntegerProperty score;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private String name;

    public IntegerProperty getScore() {
        return score;
    }

    public Player() {
        score = new SimpleIntegerProperty();
        score.set(0);
        name = "player";
    }
    public Player(String name) {
        score = new SimpleIntegerProperty(0);
        this.name = name;
    }

    public void incScore() {
        score.set(score.get() + 1);
    }

    public void reboot(){score.set(0);}
}