package sample.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    final int PORT = 1234;
    ArrayList<Connection> connections;
    ArrayList<ClientResult> results = new ArrayList<>();
    ClientResultComparator comparator = new ClientResultComparator();

    public Server() throws IOException {
        connections = new ArrayList<>();
        go();
    }

    public void go() throws IOException {
        ServerSocket s1 = new ServerSocket(PORT);
        while (true) {
            Socket client = s1.accept();
            connections.add(new Connection(this, client));
        }
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();
    }

    public void addResult(ClientResult cr) {
        results.add(cr);
        results.sort(comparator);
        clean(results);
        System.out.println(results.toString());
    }

    private void clean(ArrayList<ClientResult> results) {
        for (int i = 0; i < results.size(); i++) {
            for (int j = i + 1; j < results.size(); j++) {
                if (results.get(i).getName().equals(results.get(j).getName())) {
                    results.remove(j);
                }
            }
        }
    }

    public String getResults() {
        return results.toString();
    }
}