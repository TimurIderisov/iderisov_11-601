package sample.server;

import java.util.Comparator;

public class ClientResultComparator implements Comparator<ClientResult>{

    @Override
    public int compare(ClientResult o1, ClientResult o2) {
        if (o2.getScore() > o1.getScore()){
            return 1;
        }
        return -1;
    }
}
