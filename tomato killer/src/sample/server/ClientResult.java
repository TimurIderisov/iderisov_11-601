package sample.server;

public class ClientResult {
    private String name;

    @Override
    public String toString() {
        return name + " " + score;
    }

    public ClientResult(String name, Integer score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public Integer getScore() {
        return score;
    }

    private Integer score;

}
