package sample.server;

import java.io.*;
import java.net.Socket;

public class Connection implements Runnable {
    Socket client;
    Thread thread;
    Server server;

    public Connection(Server server, Socket client) {
        this.client = client;
        this.server = server;
        thread = new Thread(this);
        thread.start();
    }

    public void run() {
        final int PORT = 1234;
        PrintWriter writer;

        try {
            while (true) {
                InputStream is = client.getInputStream();

                int ready = is.available();
                if (ready == 0) {
                    writer = new PrintWriter(client.getOutputStream());
                    writer.write(server.getResults());
                    writer.flush();
                    writer.close();
                } else {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
                    String string = bufferedReader.readLine();
                    String[] info = string.split(" ");
                    ClientResult cr = new ClientResult(info[1], Integer.parseInt(info[0]));
                    server.addResult(cr);
                }
            }
        } catch (IOException e) {}
    }
}