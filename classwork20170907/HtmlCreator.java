import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class HtmlCreator {
    public static void main(String[] args) {
        creatingFile();
    }

    public static void creatingFile() {
        File f = new File("table.html");
        try {
            FileWriter fw = new FileWriter(f);
            String s = fillingTable();
            fw.write(s);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String fillingTable() {
        String s = new String();
        s += "<!DOCTYPE HTML> " +
                "<html> " +
                "<head> " +
                "<title>multiplication table</title> " +
                "</head> " +
                "<body> " +
                "<table> ";
        for (int i = 1; i < 10; i++) {
            s+="<tr>";
            for (int j = 1; j < 10; j++) {
                s+="<td>" + i + 'x' + j + '=' + i*j + " _-_-_-_- " + "</td>";
            }
            s+="</tr>";
        }

        s += "</table></body>" +
                "</html>;";
        return s;
    }

}
